package com.example.stjepanovic.boris.zadatak_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class DistanceActivity extends AppCompatActivity implements View.OnClickListener {

    //region Private Members

    private ArrayAdapter<CharSequence> adapter;
    private EditText etDistanceValue;
    private Button btnConvert;
    private Spinner spUnit_1, spUnit_2;
    private double result = 0;
    private Intent intent = new Intent();

    //endregion Private Members

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);
        this.initUI();
        this.initSpinner();
    }

    //region Initialization

    private void initUI() {
        this.spUnit_1 = (Spinner) findViewById(R.id.spUnit_1);
        this.spUnit_2 = (Spinner) findViewById(R.id.spUnit_2);
        this.etDistanceValue = (EditText) findViewById(R.id.etDistanceValue);
        this.btnConvert = (Button) findViewById(R.id.btnConvert);
        this.btnConvert.setOnClickListener(this);
    }

    private void initSpinner() {
        adapter = ArrayAdapter.createFromResource(this,R.array.spUnits_1, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUnit_1.setAdapter(adapter);
        spUnit_1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        spUnit_2.setAdapter(adapter);
        spUnit_2.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    //endregion Initialization

    //region Private Methods

    private double readNumber(EditText editText){
        String text = editText.getText().toString();
        return Double.parseDouble(text);
    }

    private double ConvertTo()
    {
        if(spUnit_1.getSelectedItem().toString().equals(getString(R.string.spUnit1)) && spUnit_2.getSelectedItem().toString().equals(getString(R.string.spUnit2))) {
            result = readNumber(this.etDistanceValue) / 0.0254;
        }
        else if(spUnit_1.getSelectedItem().toString().equals(getString(R.string.spUnit2)) && spUnit_2.getSelectedItem().toString().equals(getString(R.string.spUnit1))) {
            result = readNumber(this.etDistanceValue) * 0.0254;
        }
        else {
            result =  readNumber(this.etDistanceValue);
        }

        return result;
    }
    //endregion Private Methods

    //region Event Handlers

    public void onClick(View view) {
        if(!this.etDistanceValue.equals(null)) {
            intent.setClass(this, ResultActivity.class);
            intent.putExtra(ResultActivity.KEY_CONVERTFROM, spUnit_1.getSelectedItem().toString());
            intent.putExtra(ResultActivity.KEY_CONVERTTO, spUnit_2.getSelectedItem().toString());
            intent.putExtra(ResultActivity.KEY_VALUEFROM, readNumber(this.etDistanceValue));
            intent.putExtra(ResultActivity.KEY_VALUETO, ConvertTo());
            this.startActivity(intent);
        }
        else
        {
            Toast.makeText(this, R.string.textError, Toast.LENGTH_SHORT).show();
        }
    }

    //endregion Event Handlers
}
