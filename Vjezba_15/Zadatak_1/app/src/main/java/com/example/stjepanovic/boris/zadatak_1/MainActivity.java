package com.example.stjepanovic.boris.zadatak_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.content.Intent;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivTemperature;
    private ImageView ivVolume;
    private ImageView ivSpeed;
    private ImageView ivDistance;
    private Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initUI();
    }

    private void initUI()
    {
        this.ivDistance=(ImageView)findViewById(R.id.ivDistance);
        this.ivDistance.setOnClickListener(this);
        this.ivTemperature=(ImageView) findViewById(R.id.ivTemperature);
        this.ivTemperature.setOnClickListener(this);
        this.ivVolume=(ImageView)findViewById(R.id.ivVolume);
        this.ivVolume.setOnClickListener(this);
        this.ivSpeed=(ImageView)findViewById(R.id.ivSpeed);
        this.ivSpeed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.ivTemperature:
                intent.setClass(this, TemperatureActivity.class);
                startActivity(intent);
                break;
            case R.id.ivDistance:
                intent.setClass(this, DistanceActivity.class);
                startActivity(intent);
                break;
            case R.id.ivVolume:
                intent.setClass(this, VolumeActivity.class);
                startActivity(intent);
                break;
            case R.id.ivSpeed:
                intent.setClass(this, SpeedActivity.class);
                startActivity(intent);
                break;
        }
    }
}
