package com.example.stjepanovic.boris.zadatak_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class VolumeActivity extends AppCompatActivity implements View.OnClickListener  {

    //region Private Members

    private ArrayAdapter<CharSequence> adapter;
    private EditText etVolumeValue;
    private Button btnConvert;
    private Spinner spVolume_1, spVolume_2;
    private double result = 0;
    private Intent intent = new Intent();

    //endregion Private Members

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume);
        this.initUI();
        this.initSpinner();
    }

    //region Initialization

    private void initUI() {
        this.spVolume_1 = (Spinner) findViewById(R.id.spVolume_1);
        this.spVolume_2 = (Spinner) findViewById(R.id.spVolume_2);
        this.etVolumeValue = (EditText) findViewById(R.id.etVolumeValue);
        this.btnConvert = (Button) findViewById(R.id.btnConvert);
        this.btnConvert.setOnClickListener(this);
    }

    private void initSpinner() {
        adapter = ArrayAdapter.createFromResource(this,R.array.spVolumens_1, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVolume_1.setAdapter(adapter);
        spVolume_1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        spVolume_2.setAdapter(adapter);
        spVolume_2.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    //endregion Initialization

    //region Private Methods

    private double readNumber(EditText editText){
        String text = editText.getText().toString();
        return Double.parseDouble(text);
    }

    private double ConvertTo()
    {
        if(spVolume_1.getSelectedItem().toString().equals(getString(R.string.spVolume1)) && spVolume_2.getSelectedItem().toString().equals(getString(R.string.spVolume2))) {
            result = readNumber(this.etVolumeValue) * 0.00629;
        }
        else if(spVolume_1.getSelectedItem().toString().equals(getString(R.string.spVolume2)) && spVolume_2.getSelectedItem().toString().equals(getString(R.string.spVolume1))) {
            result = readNumber(this.etVolumeValue) / 0.00629;
        }
        else {
            result =  readNumber(this.etVolumeValue);
        }

        return result;
    }
    //endregion Private Methods

    //region Event Handlers

    public void onClick(View view) {
        if (this.etVolumeValue.equals(null)) {
            intent.setClass(this, ResultActivity.class);
            intent.putExtra(ResultActivity.KEY_CONVERTFROM, spVolume_1.getSelectedItem().toString());
            intent.putExtra(ResultActivity.KEY_CONVERTTO, spVolume_2.getSelectedItem().toString());
            intent.putExtra(ResultActivity.KEY_VALUEFROM, readNumber(this.etVolumeValue));
            intent.putExtra(ResultActivity.KEY_VALUETO, ConvertTo());
            this.startActivity(intent);
        } else {
            Toast.makeText(this, R.string.textError, Toast.LENGTH_SHORT).show();
        }
    }
    //endregion Event Handlers
}
