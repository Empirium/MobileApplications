package com.example.stjepanovic.boris.zadatak_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class SpeedActivity extends AppCompatActivity implements View.OnClickListener {

    //region Private Members

    private ArrayAdapter<CharSequence> adapter;
    private EditText etSpeedValue;
    private Button btnConvert;
    private Spinner spSpeed_1, spSpeed_2;
    private double result = 0;
    private Intent intent = new Intent();

    //endregion Private Members

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed);
        this.initUI();
        this.initSpinner();
    }

    //region Initialization

    private void initUI() {
        this.spSpeed_1 = (Spinner) findViewById(R.id.spSpeed_1);
        this.spSpeed_2 = (Spinner) findViewById(R.id.spSpeed_2);
        this.etSpeedValue = (EditText) findViewById(R.id.etSpeedValue);
        this.btnConvert = (Button) findViewById(R.id.btnConvert);
        this.btnConvert.setOnClickListener(this);
    }

    private void initSpinner() {
        adapter = ArrayAdapter.createFromResource(this,R.array.spSpeeds_1, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSpeed_1.setAdapter(adapter);
        spSpeed_1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        spSpeed_2.setAdapter(adapter);
        spSpeed_2.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    //endregion Initialization

    //region Private Methods

    private double readNumber(EditText editText){
        String text = editText.getText().toString();
        return Double.parseDouble(text);
    }

    private double ConvertTo()
    {
        if(spSpeed_1.getSelectedItem().toString().equals(getString(R.string.spSpeed1)) && spSpeed_2.getSelectedItem().toString().equals(getString(R.string.spSpeed2))) {
            result = readNumber(this.etSpeedValue) * 0.621372;
        }
        else if(spSpeed_1.getSelectedItem().toString().equals(getString(R.string.spSpeed2)) && spSpeed_2.getSelectedItem().toString().equals(getString(R.string.spSpeed1))) {
            result = readNumber(this.etSpeedValue) / 0.621372;
        }
        else {
            result =  readNumber(this.etSpeedValue);
        }

        return result;
    }
    //endregion Private Methods

    //region Event Handlers

    public void onClick(View view) {
        if (!this.etSpeedValue.equals(null)) {
            intent.setClass(this, ResultActivity.class);
            intent.putExtra(ResultActivity.KEY_CONVERTFROM, spSpeed_1.getSelectedItem().toString());
            intent.putExtra(ResultActivity.KEY_CONVERTTO, spSpeed_2.getSelectedItem().toString());
            intent.putExtra(ResultActivity.KEY_VALUEFROM, readNumber(this.etSpeedValue));
            intent.putExtra(ResultActivity.KEY_VALUETO, ConvertTo());
            this.startActivity(intent);
        } else {
            Toast.makeText(this, R.string.textError, Toast.LENGTH_SHORT).show();
        }
    }

    //endregion Event Handlers
}
