package com.example.stjepanovic.boris.zadatak_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TemperatureActivity extends AppCompatActivity implements View.OnClickListener {

    //region Private Members

    private ArrayAdapter<CharSequence> adapter;
    private EditText etTemperatureValue;
    private Button btnConvert;
    private Spinner spTemp_1, spTemp_2;
    private double result = 0;
    private Intent intent = new Intent();

    //endregion Private Members

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);
        this.initUI();
        this.initSpinner();
    }

    //region Initialization

    private void initUI() {
        this.spTemp_1 = (Spinner) findViewById(R.id.spTemp_1);
        this.spTemp_2 = (Spinner) findViewById(R.id.spTemp_2);
        this.etTemperatureValue = (EditText) findViewById(R.id.etTemperatureValue);
        this.btnConvert = (Button) findViewById(R.id.btnConvert);
        this.btnConvert.setOnClickListener(this);
    }

    private void initSpinner() {
        adapter = ArrayAdapter.createFromResource(this,R.array.spTemps_1, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTemp_1.setAdapter(adapter);
        spTemp_1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        spTemp_2.setAdapter(adapter);
        spTemp_2.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    //endregion Initialization

    //region Private Methods

    private double readNumber(EditText editText){
        String text = editText.getText().toString();
        return Double.parseDouble(text);
    }

    private double ConvertTo()
    {
        if(spTemp_1.getSelectedItem().toString().equals(getString(R.string.spTemp1)) && spTemp_2.getSelectedItem().toString().equals(getString(R.string.spTemp2))) {
            result = (readNumber(this.etTemperatureValue) * (1.8)) + 32;
        }
        else if(spTemp_1.getSelectedItem().toString().equals(getString(R.string.spTemp2)) && spTemp_2.getSelectedItem().toString().equals(getString(R.string.spTemp1))) {
            result = (readNumber(this.etTemperatureValue) - 32) / 1.8;
        }
        else {
            result =  readNumber(this.etTemperatureValue);
        }

        return result;
    }
    //endregion Private Methods

    //region Event Handlers

    public void onClick(View view) {
        if (this.etTemperatureValue.equals(null)) {
            intent.setClass(this, ResultActivity.class);
            intent.putExtra(ResultActivity.KEY_CONVERTFROM, spTemp_1.getSelectedItem().toString());
            intent.putExtra(ResultActivity.KEY_CONVERTTO, spTemp_2.getSelectedItem().toString());
            intent.putExtra(ResultActivity.KEY_VALUEFROM, readNumber(this.etTemperatureValue));
            intent.putExtra(ResultActivity.KEY_VALUETO, ConvertTo());
            this.startActivity(intent);
        } else {
            Toast.makeText(this, R.string.textError, Toast.LENGTH_SHORT).show();
        }
    }

    //endregion Event Handlers
}
