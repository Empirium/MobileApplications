package com.example.stjepanovic.boris.zadatak_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    //region Private Members

    private TextView tvConvertFrom, tvConvertTo, tvValueFrom, tvValueTo;

    public static final String KEY_CONVERTFROM = "convertFrom";
    public static final String KEY_CONVERTTO = "convertTo";
    public static final String KEY_VALUEFROM = "valueFrom";
    public static final String KEY_VALUETO = "valueTo";

    //endregion Private Members

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        this.initUI();
    }

    //region Initialization

    private void initUI() {
        this.tvConvertFrom = (TextView) findViewById(R.id.tvConvertFrom);
        this.tvConvertTo = (TextView) findViewById(R.id.tvConvertTo);
        this.tvValueFrom = (TextView) findViewById(R.id.tvValueFrom);
        this.tvValueTo = (TextView) findViewById(R.id.tvValueTo);
        this.handleStartingIntent(this.getIntent());
    }

    //endregion Initialization

    //region Private Methods

    private void handleStartingIntent(Intent intent) {
        if (intent != null) {
            if (intent.hasExtra(KEY_CONVERTFROM)) {
                tvConvertFrom.setText(intent.getStringExtra(KEY_CONVERTFROM));
            }
            if (intent.hasExtra(KEY_CONVERTTO)) {
                tvConvertTo.setText(intent.getStringExtra(KEY_CONVERTTO));
            }
            if (intent.hasExtra(KEY_VALUEFROM)) {
                tvValueFrom.setText(String.valueOf(intent.getDoubleExtra(KEY_VALUEFROM, 0)));
            }
            if (intent.hasExtra(KEY_VALUETO)) {
                tvValueTo.setText(String.valueOf(intent.getDoubleExtra(KEY_VALUETO, 0)));
            }
        }
    }

    //endregion Private Methods
}
