package boris.stjepanovic.example.com.zadatak_1;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.layMain) RelativeLayout layMain;
    @BindView(R.id.tvQuote) TextView tvQuote;
    @BindView(R.id.ivImage) ImageView ivImage;

    private int randCountColor, randCountTextColor, randCountQoute, randCountImage;
    private final Random mRandomGenerator = new Random();
    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            String[] colors = getResources().getStringArray(R.array.backGroundArray);
            randCountColor = mRandomGenerator.nextInt(colors.length);
            layMain.setBackgroundColor(Color.parseColor(colors[randCountColor]));

            String[] qoute = getResources().getStringArray(R.array.qouteArray);
            randCountQoute = mRandomGenerator.nextInt(qoute.length);
            tvQuote.setText(qoute[randCountQoute]);

            String[] textColors = getResources().getStringArray(R.array.textColorArray);
            randCountTextColor = mRandomGenerator.nextInt(textColors.length);
            tvQuote.setTextColor(Color.parseColor(textColors[randCountTextColor]));

            TypedArray tArray = getResources().obtainTypedArray(R.array.imageArray);
            randCountImage = mRandomGenerator.nextInt(tArray.length());
            ivImage.setImageResource(tArray.getResourceId(randCountImage, 0));
            tArray.recycle();

            changeAll();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.changeAll();
        ButterKnife.bind(this);
    }

    private int generateTime(){
        return 5000 + this.mRandomGenerator.nextInt(12000-4000);
    }

    private void changeAll(){
        mHandler.postDelayed(mRunnable, generateTime());
    }

}
