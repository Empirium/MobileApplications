package boris.stjepanovic.example.com.zadatak_2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ibImageOne) ImageButton ibImageOne;
    @BindView(R.id.ibImageTwo) ImageButton ibImageTwo;
    @BindView(R.id.ibImageThree) ImageButton ibImageThree;
    @BindView(R.id.tvDisplay) TextView tvDisplay;

    private SoundPool mSoundPool;
    private String weapon = "";
    private Bitmap bitmap;
    private boolean mIsLoaded = false;
    public static final String MSG_KEY = "msg";
    private HashMap<Integer, Integer> mSoundMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.loadSounds();

        Intent startingIntent = this.getIntent();
        if(startingIntent.hasExtra(MainActivity.MSG_KEY)){
            String message = startingIntent.getStringExtra(MainActivity.MSG_KEY);
            this.tvDisplay.setText(message);
        }
    }

    private void loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mSoundPool = new SoundPool.Builder().setMaxStreams(10).build();
        } else {
            this.mSoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        }

        this.mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                mIsLoaded = true;
            }
        });
        this.mSoundMap.put(R.raw.gun, this.mSoundPool.load(this, R.raw.gun, 1));
        this.mSoundMap.put(R.raw.rifle, this.mSoundPool.load(this, R.raw.rifle, 1));
        this.mSoundMap.put(R.raw.sniper, this.mSoundPool.load(this, R.raw.sniper, 1));
    }

    private void playSound(int resourceId) {
        int soundID = this.mSoundMap.get(resourceId);
        this.mSoundPool.play(soundID, 1,1,1,0,1F);
    }


    @OnClick({R.id.ibImageOne, R.id.ibImageTwo, R.id.ibImageThree})
    public void click(ImageButton button)
    {
        switch (button.getId()) {
            case R.id.ibImageOne:
                playSound(R.raw.gun);
                weapon = getResources().getString(R.string.textGun);
                break;
            case R.id.ibImageTwo:
                playSound(R.raw.rifle);
                weapon = getResources().getString(R.string.textRifle);
                break;
            case R.id.ibImageThree:
                playSound(R.raw.sniper);
                weapon = getResources().getString(R.string.textSniper);
                break;
        }
        this.showNoti(weapon);
    }

    private void showNoti(String direction) {
        int notificationID = 1;
        String message = getResources().getString(R.string.textMessageNotification) + " " + direction;
        String title = getResources().getString(R.string.textTitleNotification);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        switch (direction) {
            case "Pištolj":
                bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.image_gun);
                break;
            case "Puška":
                bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.image_rifle);
                break;
            case "Snajper":
                bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.image_sniper);
                break;
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MSG_KEY, direction);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(bitmap)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setLights(Color.RED, 2000, 1000)
                .setVibrate(new long[]{1000,1000,1000,1000,1000})
                .setSound(soundUri);
        Notification notification = builder.build();

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, notification);
    }
}
