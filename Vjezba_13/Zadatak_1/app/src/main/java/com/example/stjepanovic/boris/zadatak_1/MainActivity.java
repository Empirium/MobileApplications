package com.example.stjepanovic.boris.zadatak_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button bAdd, bSubtract, bDivide, bMultiply;
    EditText etFirstOperand, etSecondOperand;
    TextView tvResultsDisplay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initializeUI();
    }

    private void initializeUI() {
        this.bAdd = (Button) findViewById(R.id.bAdd);
        this.bSubtract = (Button) findViewById(R.id.bSubtract);
        this.bDivide = (Button) findViewById(R.id.bMultiply);
        this.bMultiply = (Button) findViewById(R.id.bDivide);
        this.etFirstOperand = (EditText) findViewById(R.id.etFirstOperand);
        this.etSecondOperand = (EditText) findViewById(R.id.etSecondOperand);
        this.tvResultsDisplay = (TextView) findViewById(R.id.tvResultsDisplay);
    }

    private double readOperand(EditText editText){
        String text = editText.getText().toString();
        double operand = Double.parseDouble(text);
        return operand;
    }

    private void displayResult(double result){
        this.tvResultsDisplay.setText(String.valueOf(result));
    }

    public void add(View view) {
        double operand1 = readOperand(this.etFirstOperand);
        double operand2 = readOperand(this.etSecondOperand);
        double result = operand1 + operand2;
        this.displayResult(result);
    }

    public void subtract(View view) {
        double operand1 = readOperand(this.etFirstOperand);
        double operand2 = readOperand(this.etSecondOperand);
        double result = operand1 - operand2;
        this.displayResult(result);
    }

    public void multiply(View view) {
        double operand1 = readOperand(this.etFirstOperand);
        double operand2 = readOperand(this.etSecondOperand);
        double result = operand1 * operand2;
        this.displayResult(result);
    }

    public void divide(View view) {
        double operand1 = readOperand(this.etFirstOperand);
        double operand2 = readOperand(this.etSecondOperand);
        double result = operand1 / operand2;
        this.displayResult(result);
    }

}
