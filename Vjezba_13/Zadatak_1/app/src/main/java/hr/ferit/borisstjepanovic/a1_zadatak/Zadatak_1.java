package hr.ferit.borisstjepanovic.a1_zadatak;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Zadatak_1 extends AppCompatActivity {

    private EditText etFirstNumber;
    private EditText etSecondNumber;
    private TextView tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zadatak_1);
        this.initUI();
    }

    private void initUI(){
        this.etFirstNumber = (EditText) this.findViewById(R.id.etFirstNumber);
        this.etSecondNumber = (EditText) this.findViewById(R.id.etSecondNumber);
        this.tvMessage = (TextView) this.findViewById(R.id.tvMessage);
    }

    private double readOperand(EditText editText){
        String text = editText.getText().toString();
        double operand = Double.parseDouble(text);
        return operand;
    }

    private void displayResult(double result){
        this.tvMessage.setText(String.valueOf(result));
    }

    public void addition(View view) {
        double firstNumber = readOperand(this.etFirstNumber);
        double secondNumber = readOperand(this.etSecondNumber);
        double add = firstNumber + secondNumber;
        this.displayResult(add);
    }

    public void substraction(View view) {
        double firstNumber = readOperand(this.etFirstNumber);
        double secondNumber = readOperand(this.etSecondNumber);
        double sub = firstNumber - secondNumber;
        this.displayResult(sub);
    }

    public void multiplication(View view) {
        double firstNumber = readOperand(this.etFirstNumber);
        double secondNumber = readOperand(this.etSecondNumber);
        double multi = firstNumber * secondNumber;
        this.displayResult(multi);
    }

    public void division(View view) {
        double firstNumber = readOperand(this.etFirstNumber);
        double secondNumber = readOperand(this.etSecondNumber);
        double div = firstNumber / secondNumber;
        this.displayResult(div);
    }

}
