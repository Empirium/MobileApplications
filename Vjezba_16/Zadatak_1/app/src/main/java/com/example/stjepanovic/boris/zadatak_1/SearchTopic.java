package com.example.stjepanovic.boris.zadatak_1;

/**
 * Created by bstjepanovic on 19.9.2017..
 */

public class SearchTopic {

    private String topic;

    public SearchTopic(String topic) {
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }
}
