package com.example.stjepanovic.boris.zadatak_1;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends Activity {

    @BindView(R.id.btnAddTopic) Button btnAddTopic;
    @BindView(R.id.etTopic) EditText etTopic;
    @BindView(R.id.lvTopics) ListView lvTopics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ArrayList<SearchTopic> topicsList = this.loadTopics();
        TopicAdapter adapter = new TopicAdapter(topicsList);
        this.lvTopics.setAdapter(adapter);
        this.startActivitys();
    }

    private void startActivitys() {}

    private String readNumber(EditText editText){
        return editText.getText().toString();
    }

    private ArrayList<SearchTopic> loadTopics(){
        ArrayList<SearchTopic> topics = new ArrayList<>();
        return topics;
    }
    private boolean canBeCalled(Intent implicitIntent){
        PackageManager packageManager = this.getPackageManager();
        if(implicitIntent.resolveActivity(packageManager) != null) {
            return true;
        }
        else{
            return false;
        }
    }

    @OnClick(R.id.btnAddTopic)
    public void addTopic(){
        if(readNumber(etTopic).isEmpty()) {
            Toast.makeText(this, R.string.textError, Toast.LENGTH_SHORT).show();
        }
        else
        {
            SearchTopic topic = new SearchTopic(readNumber(etTopic));
            TopicAdapter adapter = (TopicAdapter) this.lvTopics.getAdapter();
            adapter.addTopic(topic);
            etTopic.setText(null);
        }
    }

    @OnItemLongClick(R.id.lvTopics)
    public boolean removeTopic(int position)
    {
        TopicAdapter adapter = (TopicAdapter) this.lvTopics.getAdapter();
        adapter.removeTopicAt(position);
        return true;
    }


    @OnItemClick(R.id.lvTopics)
    public void search(int position){
        SearchTopic topic = (SearchTopic) this.lvTopics.getAdapter().getItem(position);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, topic.getTopic());
        if(canBeCalled(intent)){
            startActivity(intent);
        }
    }


}
