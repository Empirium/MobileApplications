package com.example.stjepanovic.boris.zadatak_1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bstjepanovic on 20.9.2017..
 */

public class TopicAdapter extends BaseAdapter {

    ArrayList<SearchTopic> topics;

    public TopicAdapter(ArrayList<SearchTopic> topics) {
        this.topics = topics;
    }

    @Override
    public int getCount() {
        return this.topics.size();
    }

    @Override
    public Object getItem(int position) {
        return this.topics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SearchTopicHolder holder;

        if(convertView == null){
            convertView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.list_item, parent, false);
            holder = new SearchTopicHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (SearchTopicHolder) convertView.getTag();
        }

        SearchTopic topic = this.topics.get(position);
        holder.tvTopicTitle.setText(topic.getTopic());

        return convertView;
    }

    public void addTopic(SearchTopic topic)
    {
        this.topics.add(topic);
        this.notifyDataSetChanged();
    }

    public void removeTopicAt(int position)
    {
        this.topics.remove(position);
        this.notifyDataSetChanged();
    }


    static class SearchTopicHolder {

        @BindView(R.id.tvTopicTitle) TextView tvTopicTitle;

        public SearchTopicHolder(View view){
            ButterKnife.bind(this, view);
        }
    }
}
