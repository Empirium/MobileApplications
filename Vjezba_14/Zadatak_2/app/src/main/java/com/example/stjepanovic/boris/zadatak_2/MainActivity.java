package com.example.stjepanovic.boris.zadatak_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private String quote;
    private ImageView ivSteve;
    private ImageView ivBill;
    private ImageView ivAlan;
    private Random rndgenerator = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initUI();
    }

    private void initUI() {
        this.ivSteve = (ImageView) this.findViewById(R.id.ivSteve);
        this.ivSteve.setOnClickListener(this);
        this.ivBill = (ImageView) this.findViewById(R.id.ivBill);
        this.ivBill.setOnClickListener(this);
        this.ivAlan = (ImageView) this.findViewById(R.id.ivAlan);
        this.ivAlan.setOnClickListener(this);
    }

    public String generateQuote(Random rndgenerator, int array)
    {
        String []quoteArray = getResources().getStringArray(array);
        int random = rndgenerator.nextInt(quoteArray.length);
        return quoteArray[random];
    }

    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.ivSteve:
                quote = this.generateQuote(rndgenerator, R.array.arraySteve);
                break;
            case R.id.ivBill:
                quote = this.generateQuote(rndgenerator, R.array.arrayBill);
                break;
            case R.id.ivAlan:
                quote = this.generateQuote(rndgenerator, R.array.arrayAlan);
                break;
        }

        Toast.makeText(this, quote, Toast.LENGTH_SHORT).show();
    }
}
