package com.example.stjepanovic.boris.zadatak_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etHeight, etWeight;
    private TextView tvResultBMI, tvResultName;
    private Button btnCalculate;
    private String category = "";
    private ImageView ivImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initUI();
    }

    private void initUI() {
        this.etHeight = (EditText) this.findViewById(R.id.etHeight);
        this.etWeight = (EditText) this.findViewById(R.id.etWeight);
        this.tvResultBMI = (TextView) this.findViewById(R.id.tvResultBMI);
        this.tvResultName = (TextView) this.findViewById(R.id.tvResultName);
        this.ivImage = (ImageView) this.findViewById(R.id.ivImage);
        this.btnCalculate = (Button) this.findViewById(R.id.btnCalculate);
        this.btnCalculate.setOnClickListener(this);
    }

    private double readNumber(EditText editText){
        String text = editText.getText().toString();
        return Double.parseDouble(text);
    }

    public double CalculateBMI() {
        double weight = readNumber(this.etWeight);
        double height = readNumber(this.etHeight);
        if(weight <= 0 || height <= 0 || weight >= 350 || height >= 2.50) {
            return 0;
        }
        else {
            return weight / (height * height);
        }
    }

    public void onClick(View view) {
        if(CalculateBMI() != 0 && CalculateBMI() < 18.5) {
            category = getString(R.string.textCategory1);
            this.ivImage.setImageResource(R.drawable.a);
        }
        else if(CalculateBMI() > 18.5 && CalculateBMI() < 25){
            category = getString(R.string.textCategory2);
            this.ivImage.setImageResource(R.drawable.b);
        }
        else if(CalculateBMI() > 25 && CalculateBMI() < 30){
            category = getString(R.string.textCategory3);
            this.ivImage.setImageResource(R.drawable.c);
        }
        else if(CalculateBMI() > 30){
            category = getString(R.string.textCategory4);
            this.ivImage.setImageResource(R.drawable.d);
        }
        else {
            Toast toast = Toast.makeText(this, "Unijeli ste podatak koji ne zadovoljava uvjete!", Toast.LENGTH_SHORT);
            toast.show();
        }

        if(CalculateBMI() != 0) {
            this.tvResultBMI.setText(String.valueOf(String.format("%.2f", CalculateBMI())));
            this.tvResultName.setText(String.valueOf(category));
        }
        else {
            this.tvResultBMI.setText(String.valueOf(""));
            this.tvResultName.setText(String.valueOf(""));
            this.ivImage.setImageResource(0);
        }
    }
}
