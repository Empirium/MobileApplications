package com.example.stjepanovic.boris.zadatak_1;

/**
 * Created by Student on 18.10.2017..
 */

public class Category {
    private long mCategoryId;
    private String mTitle;

    public Category() {
    }

    public Category( String mTitle) {
        this.mTitle = mTitle;
    }

    public Category(long mCategoryId, String mTitle) {
        this.mCategoryId = mCategoryId;
        this.mTitle = mTitle;
    }

    public long getCategoryId() {
        return mCategoryId;
    }

    public String getTitle() {
        return mTitle;
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
