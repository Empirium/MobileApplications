package com.example.stjepanovic.boris.zadatak_1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemLongClick;

public class CategoryActivity extends Activity {

    @BindView(R.id.etCategoryTitle) EditText etCategoryTitle;
    @BindView(R.id.btnAddCategory) Button btnAddCategory;
    @BindView(R.id.lvCategory) ListView lvCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        CategoryAdapter adapter = new CategoryAdapter(this);
        this.lvCategory.setAdapter(adapter);
    }

    @OnClick(R.id.btnAddCategory)
    public void addCategory(){
        if(!this.etCategoryTitle.getText().toString().equals("")) {
            Category category = new Category(this.etCategoryTitle.getText().toString());
            CategoryAdapter adapter = (CategoryAdapter) lvCategory.getAdapter();
            adapter.insertCategory(category);
        }
        else{
            Toast.makeText(this,R.string.CHOICE_ERROR,Toast.LENGTH_SHORT).show();
        }
    }

    @OnItemLongClick(R.id.lvCategory)
    public boolean onItemLongClick(int position){
        CategoryAdapter adapter = (CategoryAdapter) lvCategory.getAdapter();
        adapter.removeCategory(position);
        return false;
    }
}
