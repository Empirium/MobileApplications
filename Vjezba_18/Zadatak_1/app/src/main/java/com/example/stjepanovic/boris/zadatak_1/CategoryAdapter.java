package com.example.stjepanovic.boris.zadatak_1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Student on 19.10.2017..
 */

public class CategoryAdapter extends BaseAdapter {

    private List<Category> mCategoryList;
    private CategoryDBHelper mCategoryDBHelper;

    public CategoryAdapter(Context context) {
        mCategoryDBHelper = CategoryDBHelper.getInstance(context);
        mCategoryList = mCategoryDBHelper.retrieveCategory();
    }

    @Override
    public int getCount() {
        return this.mCategoryList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.mCategoryList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        CategoryViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.list_item_category, viewGroup, false);
            holder = new CategoryViewHolder(view);
            view.setTag(holder);
        } else{
            holder = (CategoryViewHolder) view.getTag();
        }

        Category category = this.mCategoryList.get(i);
        holder.tvCategoryTitle.setText(category.getTitle());

        return view;
    }

    static class CategoryViewHolder{
        @BindView(R.id.tvCategoryTitle) TextView tvCategoryTitle;

        public CategoryViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void insertCategory(Category category){
        this.mCategoryDBHelper.insertCategory(category);
        this.refreshData();
    }

    public void removeCategory(int position){
        this.mCategoryDBHelper.removeCategory(this.mCategoryList.get(position));
        this.refreshData();
    }

    public List<Category> retrieveCategory(){
        return this.mCategoryDBHelper.retrieveCategory();
    }

    public void refreshData(){
        this.mCategoryList = this.mCategoryDBHelper.retrieveCategory();
        this.notifyDataSetChanged();
    }
}
