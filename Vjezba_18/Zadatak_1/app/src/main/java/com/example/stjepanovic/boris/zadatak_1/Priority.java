package com.example.stjepanovic.boris.zadatak_1;

/**
 * Created by Student on 18.10.2017..
 */

public class Priority {
    private long mPriorityId;
    private String mTitle;

    public Priority() {
    }

    public Priority(String mTitle) {
        this.mTitle = mTitle;
    }
    public Priority(long mPriorityId, String mTitle) {
        this.mPriorityId = mPriorityId;
        this.mTitle = mTitle;
    }

    public long getPriorityId() {
        return mPriorityId;
    }

    public String getTitle() {
        return mTitle;
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
