package com.example.stjepanovic.boris.zadatak_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends Activity {

    @BindView(R.id.btnNewCategory) Button btnNewCategory;
    @BindView(R.id.btnNewTask) Button btnNewTask;
    @BindView(R.id.btnNewPriority) Button btnNewPriority;
    @BindView(R.id.lvTasks) ListView lvTasks;

    private Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        TaskAdapter adapter = new TaskAdapter(this);
        this.lvTasks.setAdapter(adapter);
    }

    @OnClick(R.id.btnNewTask)
    public void addNewTask() {
        intent.setClass(this, TaskActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnNewCategory)
    public void addNewCategory() {
        intent.setClass(this, CategoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnNewPriority)
    public void addNewPriority() {
        intent.setClass(this, PriorityActivity.class);
        startActivity(intent);
    }

    @OnItemLongClick(R.id.lvTasks)
    public boolean onItemLongClick(int position){
        TaskAdapter adapter = (TaskAdapter) lvTasks.getAdapter();
        adapter.removeTask(position);
        Toast.makeText(this,R.string.REMOVE_OK,Toast.LENGTH_SHORT).show();
        return false;
    }
}
