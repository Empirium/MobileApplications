package com.example.stjepanovic.boris.zadatak_1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Windows on 11/17/2017.
 */

public class PriorityAdapter extends BaseAdapter {

    private List<Priority> mPriorityList;
    private PriorityDBHelper mPriorityDBHelper;

    public PriorityAdapter(Context context) {
        mPriorityDBHelper = PriorityDBHelper.getInstance(context);
        mPriorityList = mPriorityDBHelper.retrievePriority();
    }
    @Override
    public int getCount() {
        return this.mPriorityList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.mPriorityList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        PriorityViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.list_item_priority, viewGroup, false);
            holder = new PriorityViewHolder(view);
            view.setTag(holder);
        } else{
            holder = (PriorityViewHolder) view.getTag();
        }

        Priority priority = this.mPriorityList.get(i);
        holder.tvPriorityTitle.setText(priority.getTitle());

        return view;
    }

    static class PriorityViewHolder{

        @BindView(R.id.tvPriorityTitle) TextView tvPriorityTitle;

        public PriorityViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void insertPriority(Priority priority){
        this.mPriorityDBHelper.insertPriority(priority);
        this.refreshData();
    }

    public void removePriority(int position){
        this.mPriorityDBHelper.removePriority(this.mPriorityList.get(position));
        this.refreshData();
    }

    public List<Priority> retrievePriority(){
        return this.mPriorityDBHelper.retrievePriority();
    }

    public void refreshData(){
        this.mPriorityList = this.mPriorityDBHelper.retrievePriority();
        this.notifyDataSetChanged();
    }
}
