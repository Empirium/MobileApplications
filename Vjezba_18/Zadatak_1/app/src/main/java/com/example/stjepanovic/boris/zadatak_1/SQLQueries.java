package com.example.stjepanovic.boris.zadatak_1;

/**
 * Created by bstjepanovic on 16.10.2017..
 */

public class SQLQueries {

    static class CREATE_TABLES {
        static String CREATE_TABLE_TASKS = "CREATE TABLE "
                + Schema.TABLE_NAME_TASKS + " ( "
                + Schema.TD_TASKS_ID + " INTEGER PRIMARY KEY, "
                + Schema.TD_TITLE + " TEXT UNIQUE NOT NULL, "
                + Schema.TD_PRIORITY_ID + " INTEGER REFERENCES " + Schema.TABLE_NAME_PRIORITY + " (" + Schema.PR_PRIORITY_ID + ") NOT NULL, "
                + Schema.TD_CATEGORY_ID + " INTEGER REFERENCES " + Schema.TABLE_NAME_CATEGORY + " (" + Schema.CT_CATEGORY_ID + ") NOT NULL );";

        static String CREATE_TABLE_PRIORITY = "CREATE TABLE "
                + Schema.TABLE_NAME_PRIORITY + " ( "
                + Schema.PR_PRIORITY_ID + " INTEGER PRIMARY KEY, "
                + Schema.PR_TITLE + " TEXT UNIQUE NOT NULL );";

        static String CREATE_TABLE_CATEGORY = "CREATE TABLE " + Schema.TABLE_NAME_CATEGORY + " ( "
                + Schema.CT_CATEGORY_ID + " INTEGER PRIMARY KEY, "
                + Schema.CT_TITLE + " TEXT);";
    }

    static class DROP_TABLES {
        static String DROP_TABLE_TASKS = "DROP TABLE IF EXISTS " + Schema.TABLE_NAME_TASKS;

        static String DROP_TABLE_PRIORITY = "DROP TABLE IF EXISTS " + Schema.TABLE_NAME_PRIORITY;

        static String DROP_TABLE_CATEGORY = "DROP TABLE IF EXISTS " + Schema.TABLE_NAME_CATEGORY;
    }
}
