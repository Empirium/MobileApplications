package com.example.stjepanovic.boris.zadatak_1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Student on 18.10.2017..
 */

public class TaskAdapter extends BaseAdapter {

    private List<Task> mTaskList;
    private TaskDBHelper mTaskDBHelper;

    public TaskAdapter(Context context) {
        mTaskDBHelper = TaskDBHelper.getInstance(context);
        mTaskList = mTaskDBHelper.retrieveTasks();
    }

    @Override
    public int getCount() {
        return this.mTaskList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.mTaskList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        TaskViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.list_item_tasks, viewGroup, false);
            holder = new TaskViewHolder(view);
            view.setTag(holder);
        } else{
            holder = (TaskViewHolder) view.getTag();
        }

        Task task = this.mTaskList.get(i);
        holder.tvTitle.setText(task.getTitle());
        holder.tvCategory.setText(view.getResources().getString(R.string.textCategory)  +" " + task.getCategory().getTitle());
        holder.tvPriority.setText(view.getResources().getString(R.string.textPriority)  +" " + task.getPriority().getTitle());

        return view;
    }

    static class TaskViewHolder{
        @BindView(R.id.tvTitle) TextView tvTitle;
        @BindView(R.id.tvCategory) TextView tvCategory;
        @BindView(R.id.tvPriority) TextView tvPriority;

        public TaskViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void insertTask(Task task){
        this.mTaskDBHelper.insertTask(task);
        this.refreshData();
    }

    public void removeTask(int position){
        this.mTaskDBHelper.removeTask(this.mTaskList.get(position));
        this.refreshData();
    }

    public void refreshData(){
        this.mTaskList = this.mTaskDBHelper.retrieveTasks();
        this.notifyDataSetChanged();
    }
}
