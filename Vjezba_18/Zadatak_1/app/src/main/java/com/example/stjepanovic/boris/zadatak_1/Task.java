package com.example.stjepanovic.boris.zadatak_1;

/**
 * Created by Student on 18.10.2017..
 */

public class Task {
    private String mTitle;
    private Category mCategory;
    private Priority mPriority;

    public Task() {
    }

    public Task(String mTitle, Category mCategory, Priority mPriority) {
        this.mTitle = mTitle;
        this.mCategory = mCategory;
        this.mPriority = mPriority;
    }

    public String getTitle() {
        return mTitle;
    }

    public Category getCategory() {
        return mCategory;
    }

    public Priority getPriority() {
        return mPriority;
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
