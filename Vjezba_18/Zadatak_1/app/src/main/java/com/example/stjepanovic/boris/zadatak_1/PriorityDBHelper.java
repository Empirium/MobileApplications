package com.example.stjepanovic.boris.zadatak_1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Windows on 11/17/2017.
 */

public class PriorityDBHelper extends SQLiteOpenHelper {

    private static PriorityDBHelper mPriorityDBHelper = null;

    private PriorityDBHelper(Context context){
        super(context, Schema.DATABASE_NAME, null, Schema.DATABASE_VERSION);
    }

    public static synchronized PriorityDBHelper getInstance(Context context){
        if(mPriorityDBHelper == null){
            context = context.getApplicationContext();
            mPriorityDBHelper = new PriorityDBHelper(context);
        }
        return mPriorityDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQLQueries.CREATE_TABLES.CREATE_TABLE_PRIORITY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQLQueries.DROP_TABLES.DROP_TABLE_PRIORITY);
        this.onCreate(sqLiteDatabase);
    }

    public List<Priority> retrievePriority(){
        SQLiteDatabase database = getWritableDatabase();
        String[] columns = new String[]{Schema.PR_PRIORITY_ID, Schema.PR_TITLE};
        Cursor result = database.query(Schema.TABLE_NAME_PRIORITY, columns, null, null, null, null, null);
        return parseTasks(result);
    }

    private List<Priority> parseTasks(Cursor result) {
        List<Priority> priorities = new ArrayList<>();
        if (result.moveToFirst()) {
            do {
                long priority_id = result.getLong(result.getColumnIndex(Schema.PR_PRIORITY_ID));
                String title = result.getString(result.getColumnIndex(Schema.PR_TITLE));

                Priority priority = new Priority(priority_id, title);
                priorities.add(priority);
            }
            while (result.moveToNext());
            result.close();
        }
        return priorities;
    }

    public void insertPriority(Priority priority) {
        ContentValues values = new ContentValues();
        values.put(Schema.PR_TITLE, priority.getTitle());
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert(Schema.TABLE_NAME_PRIORITY, null, values);
    }

    public void removePriority(Priority priority) {
        SQLiteDatabase database = getWritableDatabase();
        String[] whereCondition = new String[]{priority.getTitle()};
        database.delete(Schema.TABLE_NAME_PRIORITY, Schema.PR_TITLE+"=?",whereCondition);
    }
}
