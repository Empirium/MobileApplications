package com.example.stjepanovic.boris.zadatak_1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Student on 19.10.2017..
 */

public class CategoryDBHelper extends SQLiteOpenHelper{

    private static CategoryDBHelper mCategoryDBHelper = null;

    private CategoryDBHelper(Context context){
        super(context, Schema.DATABASE_NAME, null, Schema.DATABASE_VERSION);
    }

    public static synchronized CategoryDBHelper getInstance(Context context){
        if(mCategoryDBHelper == null){
            context = context.getApplicationContext();
            mCategoryDBHelper = new CategoryDBHelper(context);
        }
        return mCategoryDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQLQueries.CREATE_TABLES.CREATE_TABLE_CATEGORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQLQueries.DROP_TABLES.DROP_TABLE_CATEGORY);
        this.onCreate(sqLiteDatabase);
    }

    public List<Category> retrieveCategory(){
        SQLiteDatabase database = getWritableDatabase();
        String[] columns = new String[]{Schema.CT_CATEGORY_ID, Schema.CT_TITLE};
        Cursor result = database.query(Schema.TABLE_NAME_CATEGORY, columns, null, null, null, null, null);
        return parseTasks(result);
    }

    private List<Category> parseTasks(Cursor result) {
        List<Category> categories = new ArrayList<>();
        if (result.moveToFirst()) {
            do {
                long category_id = result.getLong(result.getColumnIndex(Schema.CT_CATEGORY_ID));
                String title = result.getString(result.getColumnIndex(Schema.CT_TITLE));

                Category category = new Category(category_id, title);
                categories.add(category);
            }
            while (result.moveToNext());
            result.close();
        }
        return categories;
    }
    public void insertCategory(Category category) {
        ContentValues values = new ContentValues();
        values.put(Schema.CT_TITLE, category.getTitle());
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert(Schema.TABLE_NAME_CATEGORY, null, values);
    }

    public void removeCategory(Category category) {
        SQLiteDatabase database = getWritableDatabase();
        String[] whereCondition = new String[]{category.getTitle()};
        database.delete(Schema.TABLE_NAME_CATEGORY, Schema.CT_TITLE+"=?",whereCondition);
    }
}
