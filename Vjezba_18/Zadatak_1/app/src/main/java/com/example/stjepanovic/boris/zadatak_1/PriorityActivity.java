package com.example.stjepanovic.boris.zadatak_1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemLongClick;

public class PriorityActivity extends Activity {

    @BindView(R.id.etPriorityTitle) EditText etPriorityTitle;
    @BindView(R.id.btnAddPriority) Button btnAddPriority;
    @BindView(R.id.lvPriority) ListView lvPriority;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_priority);
        ButterKnife.bind(this);
        PriorityAdapter adapter = new PriorityAdapter(this);
        this.lvPriority.setAdapter(adapter);
    }

    @OnClick(R.id.btnAddPriority)
    public void addPriority(){
        if(!this.etPriorityTitle.getText().toString().equals("")) {
            Priority priority = new Priority(this.etPriorityTitle.getText().toString());
            PriorityAdapter adapter = (PriorityAdapter) lvPriority.getAdapter();
            adapter.insertPriority(priority);
        }
        else{
            Toast.makeText(this,R.string.CHOICE_ERROR,Toast.LENGTH_SHORT).show();
        }
    }

    @OnItemLongClick(R.id.lvPriority)
    public boolean onItemLongClick(int position){
        PriorityAdapter adapter = (PriorityAdapter) lvPriority.getAdapter();
        adapter.removePriority(position);
        return false;
    }
}
