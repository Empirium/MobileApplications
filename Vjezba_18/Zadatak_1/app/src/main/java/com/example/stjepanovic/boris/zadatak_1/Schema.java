package com.example.stjepanovic.boris.zadatak_1;

/**
 * Created by bstjepanovic on 16.10.2017..
 */

public class Schema {
    // Database info:
    public static String DATABASE_NAME = "database.db";
    public static int DATABASE_VERSION = 1;

    // Database tables:
    public static String TABLE_NAME_TASKS = "TD_TASKS";
    public static String TABLE_NAME_CATEGORY = "TD_CATEGORY";
    public static String TABLE_NAME_PRIORITY = "TD_PRIORITY";

    // Table column names:
    // TASKS:
    public static String TD_TASKS_ID = "TASKS_ID";
    public static String TD_TITLE = "TITLE";
    public static String TD_PRIORITY_ID = "PRIORITY_ID";
    public static String TD_CATEGORY_ID = "CATEGORY_ID";

    // PRIORITY:
    public static String PR_PRIORITY_ID = "PRIORITY_ID";
    public static String PR_TITLE = "PRIORITY_TITLE";

    // CATEGORY:
    public static String CT_CATEGORY_ID = "CATEGORY_ID";
    public static String CT_TITLE = "CATEGORY_TITLE";
}
