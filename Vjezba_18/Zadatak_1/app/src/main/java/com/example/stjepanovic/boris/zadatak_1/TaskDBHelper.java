package com.example.stjepanovic.boris.zadatak_1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Student on 18.10.2017..
 */

public class TaskDBHelper extends SQLiteOpenHelper {

    private static TaskDBHelper mTaskDBHelper = null;

    private TaskDBHelper(Context context){
        super(context, Schema.DATABASE_NAME, null, Schema.DATABASE_VERSION);
    }

    public static synchronized TaskDBHelper getInstance(Context context){

        if(mTaskDBHelper == null){
            context = context.getApplicationContext();
            mTaskDBHelper = new TaskDBHelper(context);
        }
        return mTaskDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQLQueries.CREATE_TABLES.CREATE_TABLE_CATEGORY);
        sqLiteDatabase.execSQL(SQLQueries.CREATE_TABLES.CREATE_TABLE_PRIORITY);
        sqLiteDatabase.execSQL(SQLQueries.CREATE_TABLES.CREATE_TABLE_TASKS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQLQueries.DROP_TABLES.DROP_TABLE_CATEGORY);
        sqLiteDatabase.execSQL(SQLQueries.DROP_TABLES.DROP_TABLE_PRIORITY);
        sqLiteDatabase.execSQL(SQLQueries.DROP_TABLES.DROP_TABLE_TASKS);
        this.onCreate(sqLiteDatabase);
    }

    public void insertTask(Task task) {
        ContentValues values = new ContentValues();
        values.put(Schema.TD_TITLE, task.getTitle());
        values.put(Schema.TD_PRIORITY_ID, task.getPriority().getPriorityId());
        values.put(Schema.TD_CATEGORY_ID, task.getCategory().getCategoryId());
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert(Schema.TABLE_NAME_TASKS, null, values);
    }

    public void removeTask(Task task) {
        SQLiteDatabase database = getWritableDatabase();
        String[] whereCondition = new String[]{task.getTitle()};
        database.delete(Schema.TABLE_NAME_TASKS, Schema.TD_TITLE+"=?",whereCondition);
    }

    public Category getCategory(long category_id) {
        SQLiteDatabase database = this.getWritableDatabase();
        String[] columns = new String[]{Schema.CT_CATEGORY_ID, Schema.CT_TITLE};
        Cursor result = database.query(Schema.TABLE_NAME_CATEGORY, columns, Schema.CT_CATEGORY_ID + " = " + category_id, null, null, null, null);
        return parseTasks3(result);
    }

    private Category parseTasks3(Cursor result) {
        Category category = new Category();
        if (result.moveToFirst()) {
            do {
                long category_id = result.getLong(result.getColumnIndex(Schema.CT_CATEGORY_ID));
                String title = result.getString(result.getColumnIndex(Schema.CT_TITLE));

                category = new Category(category_id, title);
            }
            while (result.moveToNext());
            result.close();
        }
        return category;
    }

    public Priority getPriority(long priority_id) {
        SQLiteDatabase database = this.getWritableDatabase();
        String[] columns = new String[]{Schema.PR_PRIORITY_ID, Schema.PR_TITLE};
        Cursor result = database.query(Schema.TABLE_NAME_PRIORITY, columns, Schema.PR_PRIORITY_ID + " = " + priority_id, null, null, null, null);
        return parseTasks2(result);
    }

    private Priority parseTasks2(Cursor result) {
        Priority priority = new Priority();
        if (result.moveToFirst()) {
            do {
                long priority_id = result.getLong(result.getColumnIndex(Schema.PR_PRIORITY_ID));
                String title = result.getString(result.getColumnIndex(Schema.PR_TITLE));

                priority = new Priority(priority_id, title);
            }
            while (result.moveToNext());
            result.close();
        }
        return priority;
    }

    public List<Task> retrieveTasks(){
        SQLiteDatabase database = getWritableDatabase();
        String[] columns = new String[]{Schema.TD_TITLE, Schema.TD_CATEGORY_ID, Schema.TD_PRIORITY_ID};
        Cursor result = database.query(Schema.TABLE_NAME_TASKS,columns,null,null,null,null,null);
        return parseTasks(result);
    }

    private List<Task> parseTasks(Cursor result) {
        List<Task> tasks = new ArrayList<>();
        if (result.moveToFirst()) {
            do {
                String title = result.getString(result.getColumnIndex(Schema.TD_TITLE));
                Task task = new Task(title, getCategory(result.getLong(result.getColumnIndex(Schema.TD_CATEGORY_ID))), getPriority(result.getLong(result.getColumnIndex(Schema.TD_PRIORITY_ID))));
                tasks.add(task);
            }
            while (result.moveToNext());
            result.close();
        }
        return tasks;
    }
}
