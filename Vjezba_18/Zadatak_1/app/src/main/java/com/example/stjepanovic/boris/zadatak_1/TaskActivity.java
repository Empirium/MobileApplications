package com.example.stjepanovic.boris.zadatak_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TaskActivity extends Activity {

    @BindView(R.id.etTaskTitle) EditText etTaskTitle;
    @BindView(R.id.btnAddTask) Button btnAddTask;
    @BindView(R.id.spPriority) Spinner spPriority;
    @BindView(R.id.spCategory) Spinner spCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        ButterKnife.bind(this);
        this.initSpinner();
    }

    private void initSpinner() {
        CategoryAdapter catAdapter =  new CategoryAdapter(getApplicationContext());
        List<Category> category = catAdapter.retrieveCategory();
        ArrayAdapter<Category> categoryArrayAdapter = new ArrayAdapter<Category>(this, android.R.layout.simple_spinner_item, category);
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategory.setAdapter(categoryArrayAdapter);

        PriorityAdapter priAdapter =  new PriorityAdapter(getApplicationContext());
        List<Priority> priority = priAdapter.retrievePriority();
        ArrayAdapter<Priority> priorityArrayAdapter = new ArrayAdapter<Priority>(this, android.R.layout.simple_spinner_item, priority);
        priorityArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPriority.setAdapter(priorityArrayAdapter);
        spPriority.getOnItemSelectedListener();
    }

    @OnClick(R.id.btnAddTask)
    public void addTask(){
        if(!this.etTaskTitle.getText().toString().equals("")) {
            String taskTitle = this.etTaskTitle.getText().toString();
            Category category = (Category) spCategory.getSelectedItem();
            Priority priority = (Priority) spPriority.getSelectedItem();

            Task task = new Task(taskTitle, category, priority);

            TaskAdapter adapter = new TaskAdapter(getApplicationContext());
            adapter.insertTask(task);

            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(this,R.string.CHOICE_ERROR,Toast.LENGTH_SHORT).show();
        }
    }
}
