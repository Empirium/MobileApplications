package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bstjepanovic on 25.9.2017..
 */

public class JSONRain {

    @SerializedName("3h") private double m3hRain;

    public JSONRain() {
    }

    public JSONRain(double m3hRain) {
        this.m3hRain = m3hRain;
    }

    public double get3hRain() {
        return m3hRain;
    }

}

