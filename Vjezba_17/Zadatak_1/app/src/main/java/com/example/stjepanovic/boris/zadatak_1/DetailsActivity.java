package com.example.stjepanovic.boris.zadatak_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bstjepanovic on 26.9.2017..
 */

public class DetailsActivity extends Activity {

    public static final String KEY_DATE = "date";
    public static final String KEY_TEMP = "temp";
    public static final String KEY_HUMIDITY = "humidity";
    public static final String KEY_PRESSURE = "pressure";
    public static final String KEY_WIND = "windSpeed";


    @BindView(R.id.tvDateDetails) TextView tvDateDetails;
    @BindView(R.id.tvTemp) TextView tvTemp;
    @BindView(R.id.tvHumidity) TextView tvHumidity;
    @BindView(R.id.tvPressure) TextView tvPressure;
    @BindView(R.id.tvWind) TextView tvWind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        this.handleStartingIntent(this.getIntent());
    }

    private void handleStartingIntent(Intent intent) {
        if (intent != null) {
            if (intent.hasExtra(KEY_DATE)) {
                tvDateDetails.setText(intent.getStringExtra(KEY_DATE));
            }
            if (intent.hasExtra(KEY_TEMP)) {
                tvTemp.setText(intent.getStringExtra(KEY_TEMP));
            }
            if (intent.hasExtra(KEY_HUMIDITY)) {
                tvHumidity.setText(intent.getStringExtra(KEY_HUMIDITY));
            }
            if (intent.hasExtra(KEY_PRESSURE)) {
                tvPressure.setText(intent.getStringExtra(KEY_PRESSURE));
            }
            if(intent.hasExtra(KEY_WIND)){
                tvWind.setText(intent.getStringExtra(KEY_WIND));
            }
        }
    }

}
