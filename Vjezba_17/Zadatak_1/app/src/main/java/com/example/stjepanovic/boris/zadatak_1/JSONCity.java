package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bstjepanovic on 25.9.2017..
 */

class JSONCity {

    @SerializedName("id") private int mId;
    @SerializedName("name") private String mCityName;
    @SerializedName("coord") private JSONCoordinate mJSONCoordinate;
    @SerializedName("country") private String mCountry;

    public JSONCity() {
    }

    public JSONCity(int mId, String mCityName, JSONCoordinate mJSONCoordinate, String mCountry) {
        this.mId = mId;
        this.mCityName = mCityName;
        this.mJSONCoordinate = mJSONCoordinate;
        this.mCountry = mCountry;
    }

    public int getmId() {
        return mId;
    }

    public String getmCityName() {
        return mCityName;
    }

    public JSONCoordinate getmJSONCoordinate() {
        return mJSONCoordinate;
    }

    public String getmCountry() {
        return mCountry;
    }
}
