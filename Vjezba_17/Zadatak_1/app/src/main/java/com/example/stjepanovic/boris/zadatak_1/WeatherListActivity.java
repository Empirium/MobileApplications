package com.example.stjepanovic.boris.zadatak_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherListActivity extends Activity implements Callback<JSONList> {

    public static final String QUERY = "query";
    public static final String SPINNERQUERY = "spinnerQuery";
    public static final String COORDLAT = "coordLat";
    public static final String COORDLON = "coordLon";

    @BindView(R.id.lvTvShows) ListView lvTvShows;
    @BindView(R.id.tvCityName) TextView tvCityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_list);
        ButterKnife.bind(this);
        this.handleStartingIntent(this.getIntent());
    }

    private void handleStartingIntent(Intent intent) {
        if (intent != null) {
            if (intent.hasExtra(QUERY)) {
                this.loadTvShowInfo(getIntent().getStringExtra(QUERY), null, null);
            }
            else if (intent.hasExtra(SPINNERQUERY)) {
                this.loadTvShowInfo(getIntent().getStringExtra(SPINNERQUERY), null, null);
            }
            else if (intent.hasExtra(COORDLAT) && intent.hasExtra(COORDLON)) {
                this.loadTvShowInfo( null, getIntent().getStringExtra(COORDLAT), getIntent().getStringExtra(COORDLON));
            }
        }
    }

    private void loadTvShowInfo(String tvShowName, String coordinateLat, String coordinateLon) {
        Retrofit retrofit = new  Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl(OpenWeatherMapAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        OpenWeatherMapAPI tvMazeAPI = retrofit.create(OpenWeatherMapAPI.class);
        Call<JSONList> request;
        if(tvShowName != null) {
            request = tvMazeAPI.getWeatherAPI(tvShowName, OpenWeatherMapAPI.UNIT, OpenWeatherMapAPI.APPID_KEY);
        } else {
            request = tvMazeAPI.getWeatherCoordinateAPI(coordinateLat, coordinateLon, OpenWeatherMapAPI.UNIT, OpenWeatherMapAPI.APPID_KEY);
        }
        request.enqueue(this);
    }

    @Override
    public void onResponse(Call<JSONList> call, Response<JSONList> response) {
        JSONList showList = response.body();
        ShowAdapter adapter = new ShowAdapter(showList);
        lvTvShows.setAdapter(adapter);
        tvCityName.setText(showList.getJSONCity().getmCityName());
    }

    @Override
    public void onFailure(Call<JSONList> call, Throwable t) {
        Log.e("ERROR", t.getMessage());
        Toast.makeText(this,R.string.API_ERROR,Toast.LENGTH_SHORT).show();
    }

    @OnItemClick(R.id.lvTvShows)
    public void onListItemClick(int position){
        JSONWeatherList weather = (JSONWeatherList) this.lvTvShows.getAdapter().getItem(position);
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_DATE, weather.getDtText().toString());
        intent.putExtra(DetailsActivity.KEY_TEMP, String.valueOf(weather.getMain().getTemp()));
        intent.putExtra(DetailsActivity.KEY_HUMIDITY, String.valueOf(weather.getMain().getHumidity()));
        intent.putExtra(DetailsActivity.KEY_PRESSURE, String.valueOf(weather.getMain().getPressure()));
        intent.putExtra(DetailsActivity.KEY_WIND, String.valueOf(weather.getWind().getSpeed()));
        this.startActivity(intent);
    }
}
