package com.example.stjepanovic.boris.zadatak_1;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenWeatherMapAPI {
    String BASE_URL = "http://api.openweathermap.org/";
    String UNIT = "metric";
    String APPID_KEY = "f313d48314997175140453598b0d99ac";
    String IMAGE_URL = "http://openweathermap.org/img/w/";

    @GET("data/2.5/forecast")
        Call<JSONList> getWeatherAPI(
                @Query("q") String cityName,
                @Query("units") String unit,
                @Query("APPID") String apiKey);

    @GET("data/2.5/forecast")
        Call<JSONList> getWeatherCoordinateAPI(
            @Query("lat") String coordinateLat,
            @Query("lon") String coordinateLon,
            @Query("units") String unit,
            @Query("APPID") String apiKey);
}
