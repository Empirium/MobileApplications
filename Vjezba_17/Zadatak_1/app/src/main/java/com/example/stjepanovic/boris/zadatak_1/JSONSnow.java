package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Windows on 10/1/2017.
 */

public class JSONSnow {

    @SerializedName("3h") private double m3hSnow;

    public JSONSnow() {
    }

    public JSONSnow(double m3hSnow) {
        this.m3hSnow = m3hSnow;
    }

    public double get3hSnow() {
        return m3hSnow;
    }
}
