package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Windows on 9/24/2017.
 */

class JSONSys {

    @SerializedName("pod") private String mPod;

    public JSONSys() {
    }

    public JSONSys(String mPod) {
        this.mPod = mPod;
    }

    public String getPod() {
        return mPod;
    }
}
