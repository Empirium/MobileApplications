package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Windows on 9/24/2017.
 */

class JSONWind {

    @SerializedName("speed") private double mSpeed;
    @SerializedName("deg") private double mDeg;

    public JSONWind() {
    }

    public JSONWind(double mSpeed, double mDeg) {
        this.mSpeed = mSpeed;
        this.mDeg = mDeg;
    }

    public double getSpeed() {
        return mSpeed;
    }

    public double getDeg() {
        return mDeg;
    }
}
