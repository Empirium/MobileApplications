package com.example.stjepanovic.boris.zadatak_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.BindView;
import butterknife.OnItemSelected;

public class MainActivity  extends Activity {

    private ArrayAdapter<CharSequence> adapter;

    @BindView(R.id.etShowNameEntry) EditText etShowNameEntry;
    @BindView(R.id.ibSearch) Button ibSearch;
    @BindView(R.id.spCity) Spinner spCity;
    @BindView(R.id.etCoordinateLat) EditText etCoordinateLat;
    @BindView(R.id.etCoordinateLon) EditText etCoordinateLon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.initSpinner();
    }

    private void initSpinner() {
        adapter = ArrayAdapter.createFromResource(this, R.array.spArrayCity, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(adapter);
    }

    @OnClick(R.id.ibSearch)
    public void searchCurrentWeather() {
        String query = etShowNameEntry.getText().toString();
        String spinnerQuery = spCity.getSelectedItem().toString();
        String coordinateLat = String.valueOf(etCoordinateLat.getText());
        String coordinateLon = String.valueOf(etCoordinateLon.getText());

        Intent intent = new Intent(this, WeatherListActivity.class);

        if (!query.equals("")) {
            intent.putExtra(WeatherListActivity.QUERY, query);
            this.startActivity(intent);
        } else if (!spinnerQuery.equals("")) {
            intent.putExtra(WeatherListActivity.SPINNERQUERY, spinnerQuery);
            this.startActivity(intent);
        } else if (!coordinateLat.equals("") && !coordinateLon.equals("")) {
            intent.putExtra(WeatherListActivity.COORDLAT, coordinateLat);
            intent.putExtra(WeatherListActivity.COORDLON, coordinateLon);
            this.startActivity(intent);
        } else{
            Toast.makeText(this,R.string.CHOICE_ERROR,Toast.LENGTH_SHORT).show();
        }
    }
}
