package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Windows on 9/24/2017.
 */

class JSONClouds {

    @SerializedName("all") private int mAll;

    public JSONClouds() {
    }

    public JSONClouds(int mAll) {
        this.mAll = mAll;
    }

    public int getAll() {
        return mAll;
    }
}
