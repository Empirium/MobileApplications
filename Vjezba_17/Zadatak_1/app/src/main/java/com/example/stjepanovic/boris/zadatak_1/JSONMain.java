package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by Windows on 9/24/2017.
 */

class JSONMain {

    @SerializedName("temp") private double mTemp;
    @SerializedName("temp_min") private double mTempMin;
    @SerializedName("temp_max") private double mTempMax;
    @SerializedName("pressure") private double mPressure;
    @SerializedName("sea_level") private double mSeaLevel;
    @SerializedName("grnd_level") private double mGrndLevel;
    @SerializedName("humidity") private int mHumidity;
    @SerializedName("temp_kf") private BigDecimal mTempKf;

    public JSONMain() {
    }

    public JSONMain(double mTemp, double mTempMin, double mTempMax, double mPressure, double mSeaLevel, double mGrndLevel, int mHumidity, BigDecimal mTempKf) {

        this.mTemp = mTemp;
        this.mTempMin = mTempMin;
        this.mTempMax = mTempMax;
        this.mPressure = mPressure;
        this.mSeaLevel = mSeaLevel;
        this.mGrndLevel = mGrndLevel;
        this.mHumidity = mHumidity;
        this.mTempKf = mTempKf;
    }

    public double getTemp() {
        return mTemp;
    }

    public double getTempMin() {
        return mTempMin;
    }

    public double getTempMax() {
        return mTempMax;
    }

    public double getPressure() {
        return mPressure;
    }

    public double getSeaLevel() {
        return mSeaLevel;
    }

    public double getGrndLevel() {
        return mGrndLevel;
    }

    public int getHumidity() {
        return mHumidity;
    }

    public BigDecimal getTempKf() {
        return mTempKf;
    }
}
