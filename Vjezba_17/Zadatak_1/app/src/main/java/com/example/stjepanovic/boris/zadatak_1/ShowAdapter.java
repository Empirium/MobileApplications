package com.example.stjepanovic.boris.zadatak_1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Windows on 9/24/2017.
 */

public class ShowAdapter extends BaseAdapter {

    JSONList mJSONList;

    public ShowAdapter(JSONList mJSONList) {
        this.mJSONList = mJSONList;
    }

    @Override
    public int getCount() {
        return this.mJSONList.getJSONWeatherList().size();
    }

    @Override
    public Object getItem(int position) {
        return this.mJSONList.getJSONWeatherList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ShowViewHolder holder;

        if(convertView == null)
        {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_weather, parent, false);
            holder = new ShowViewHolder(convertView);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ShowViewHolder) convertView.getTag();
        }

        JSONWeatherList list = this.mJSONList.getJSONWeatherList().get(position);
        holder.tvDate.setText(list.getDtText());
        holder.tvTempMin.setText("Temp. (min): " + String.valueOf(list.getMain().getTempMin()));
        holder.tvTempMax.setText("Temp. (max): " + String.valueOf(list.getMain().getTempMax()));

        for (JSONWeather JSONWeather : list.getWeather()) {

            holder.tvWeatherMain.setText(JSONWeather.getMain());
            holder.tvDescription.setText(JSONWeather.getDescription());


            Picasso.with(parent.getContext())
                    .load(OpenWeatherMapAPI.IMAGE_URL + JSONWeather.getIcon() + ".png")
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_unavailable)
                    .into(holder.ivWeatherIcon);
        }

        return convertView;
    }

    static class ShowViewHolder{
        @BindView(R.id.ivWeatherIcon) ImageView ivWeatherIcon;
        @BindView(R.id.tvWeatherMain) TextView tvWeatherMain;
        @BindView(R.id.tvDescription) TextView tvDescription;
        @BindView(R.id.tvDate) TextView tvDate;
        @BindView(R.id.tvTempMin) TextView tvTempMin;
        @BindView(R.id.tvTempMax) TextView tvTempMax;

        public ShowViewHolder(View view){
            ButterKnife.bind(this,view);
        }
    }

}
