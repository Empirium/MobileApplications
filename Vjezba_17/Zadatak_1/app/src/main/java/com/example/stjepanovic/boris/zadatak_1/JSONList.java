package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Windows on 9/24/2017.
 */

public class JSONList {
    @SerializedName("cod") private int mCod;
    @SerializedName("message") private double mMessage;
    @SerializedName("cnt") private int mCnt;
    @SerializedName("list") private List<JSONWeatherList> mJSONWeatherList;
    @SerializedName("city") private JSONCity mJSONCity;

    public JSONList() {
    }

    public JSONList(int mCod, double mMessage, int mCnt, List<JSONWeatherList> mJSONWeatherList, JSONCity mJSONCity) {
        this.mCod = mCod;
        this.mMessage = mMessage;
        this.mCnt = mCnt;
        this.mJSONWeatherList = mJSONWeatherList;
        this.mJSONCity = mJSONCity;
    }

    public int getCod() {
        return mCod;
    }

    public double getMessage() {
        return mMessage;
    }

    public int getCnt() {
        return mCnt;
    }

    public List<JSONWeatherList> getJSONWeatherList() {
        return mJSONWeatherList;
    }

    public JSONCity getJSONCity() {
        return mJSONCity;
    }
}
