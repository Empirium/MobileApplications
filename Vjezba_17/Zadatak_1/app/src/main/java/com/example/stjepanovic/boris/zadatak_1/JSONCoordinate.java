package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bstjepanovic on 25.9.2017..
 */

class JSONCoordinate {
    @SerializedName("lat") private double mLat;
    @SerializedName("lon") private double mLon;

    public JSONCoordinate() {
    }

    public JSONCoordinate(double mLat, double mLon) {
        this.mLat = mLat;
        this.mLon = mLon;
    }

    public double getmLat() {
        return mLat;
    }

    public double getmLon() {
        return mLon;
    }
}
