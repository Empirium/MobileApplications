package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Windows on 9/24/2017.
 */

class JSONWeather {

    @SerializedName("id") private int mId;
    @SerializedName("main") private String mMain;
    @SerializedName("description") private String mDescription;
    @SerializedName("icon") private String mIcon;

    public JSONWeather() {
    }

    public JSONWeather(int mId, String mMain, String mDescription, String mIcon) {
        this.mId = mId;
        this.mMain = mMain;
        this.mDescription = mDescription;
        this.mIcon = mIcon;
    }

    public int getId() {
        return mId;
    }

    public String getMain() {
        return mMain;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getIcon() {
        return mIcon;
    }
}
