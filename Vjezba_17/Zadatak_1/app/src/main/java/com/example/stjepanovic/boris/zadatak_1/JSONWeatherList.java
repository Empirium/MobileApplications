package com.example.stjepanovic.boris.zadatak_1;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Windows on 9/24/2017.
 */

class JSONWeatherList {

    @SerializedName("dt") private int mDt;
    @SerializedName("main") private JSONMain mJSONMain;
    @SerializedName("weather") private List<JSONWeather> mJSONWeather;
    @SerializedName("clouds") private JSONClouds mJSONClouds;
    @SerializedName("wind") private JSONWind mJSONWind;
    @SerializedName("rain") private JSONRain mJSONRain;
    @SerializedName("snow") private JSONSnow mJSONSnow;
    @SerializedName("sys") private JSONSys mJSONSys;
    @SerializedName("dt_txt") private String mDtText;

    public JSONWeatherList() {
    }

    public JSONWeatherList(int mDt, JSONMain mJSONMain, List<JSONWeather> mJSONWeather, JSONClouds mJSONClouds, JSONWind mJSONWind, JSONRain mJSONRain, JSONSnow mJSONSnow, JSONSys mJSONSys, String mDtText) {
        this.mDt = mDt;
        this.mJSONMain = mJSONMain;
        this.mJSONWeather = mJSONWeather;
        this.mJSONClouds = mJSONClouds;
        this.mJSONWind = mJSONWind;
        this.mJSONRain = mJSONRain;
        this.mJSONSnow = mJSONSnow;
        this.mJSONSys = mJSONSys;
        this.mDtText = mDtText;
    }

    public int getDt() {
        return mDt;
    }

    public JSONMain getMain() {
        return mJSONMain;
    }

    public List<JSONWeather> getWeather() {
        return mJSONWeather;
    }

    public JSONClouds getClouds() {
        return mJSONClouds;
    }

    public JSONWind getWind() {
        return mJSONWind;
    }

    public JSONRain getRain() {
        return mJSONRain;
    }

    public JSONSnow getSnow() {
        return mJSONSnow;
    }

    public JSONSys getSys() {
        return mJSONSys;
    }

    public String getDtText() {
        return mDtText;
    }
}
