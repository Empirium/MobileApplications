package com.example.stjepanovic.boris.zadatak_2;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by bstjepanovic on 11.10.2017..
 */

@Root(name="channel", strict = false)
public class BugChannel {
    @ElementList(inline = true, name = "item") private List<BugFeedItem> mItems;

    public BugChannel() {}

    public BugChannel(List<BugFeedItem> items) {
        mItems = items;
    }

    public List<BugFeedItem> getItems() {
        return mItems;
    }

}
