package com.example.stjepanovic.boris.zadatak_2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bstjepanovic on 11.10.2017..
 */

public class BugNewsAdapter extends BaseAdapter {

    List<BugFeedItem> mNewsItems;

    public BugNewsAdapter(List<BugFeedItem> mNewsItems) {
        this.mNewsItems = mNewsItems;
    }

    @Override
    public int getCount() {
        return mNewsItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mNewsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NewsItemHolder itemHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_news, parent, false);
            itemHolder = new NewsItemHolder(convertView);
            convertView.setTag(itemHolder);
        }else {
            itemHolder = (NewsItemHolder) convertView.getTag();
        }

        BugFeedItem newsItem = this.mNewsItems.get(position);

        itemHolder.tvNewsDate.setText(newsItem.getmDate());
        itemHolder.tvNewsTitle.setText(newsItem.getmTitle());
        itemHolder.tvNewsDescription.setText(newsItem.getmDescription());
        Picasso.with(parent.getContext())
                .load(newsItem.getmUrl().getmURL())
                .fit()
                .centerCrop()
                .error(R.drawable.image_unavailable)
                .placeholder(R.drawable.image_placeholder)
                .into(itemHolder.ivNewsThumbnail);

        return convertView;
    }

    static class NewsItemHolder{
        @BindView(R.id.tvNewsDate) TextView tvNewsDate;
        @BindView(R.id.tvNewsTitle) TextView tvNewsTitle;
        @BindView(R.id.tvNewsdescription) TextView tvNewsDescription;
        @BindView(R.id.ivNewsThumbnail) ImageView ivNewsThumbnail;

        public NewsItemHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }

}
