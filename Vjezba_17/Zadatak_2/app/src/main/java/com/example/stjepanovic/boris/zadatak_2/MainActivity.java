package com.example.stjepanovic.boris.zadatak_2;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class MainActivity extends Activity implements Callback<BugFeed> {


    private ArrayAdapter<CharSequence> adapter;

    @BindView(R.id.lvNews) ListView lvNewsList;
    @BindView(R.id.spCategory) Spinner spCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.loadData();
        this.initSpinner();
    }

    private void initSpinner() {
        adapter = ArrayAdapter.createFromResource(this, R.array.spArrayCategory, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategory.setAdapter(adapter);
        spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                loadData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    private void loadData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BugAPI.BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        BugAPI bugAPI = retrofit.create(BugAPI.class);
        Call<BugFeed> call = bugAPI.getNews();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<BugFeed> call, Response<BugFeed> response) {
        BugChannel feed = response.body().getChannel();
        List<BugFeedItem> items = feed.getItems();
        for (int i = 0; i < items.size(); i++) {
            if(spCategory.getSelectedItem().equals("")){
                break;
            }
            else if (!spCategory.getSelectedItem().equals(items.get(i).getmCategory())){
                items.remove(i);
                i--;
            }
        }

        BugNewsAdapter adapter = new BugNewsAdapter(items);
        lvNewsList.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<BugFeed> call, Throwable t) {
        Log.e("Error", t.getMessage());
        Toast.makeText(this, R.string.API_ERROR, Toast.LENGTH_SHORT).show();
    }

    @OnItemClick(R.id.lvNews)
    public void onListItemClick(int position){
        BugFeedItem item = (BugFeedItem) this.lvNewsList.getAdapter().getItem(position);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, item.getmMore());
        if(canBeCalled(intent)){
            startActivity(intent);
        }
    }

    private boolean canBeCalled(Intent implicitIntent){
        PackageManager packageManager = this.getPackageManager();
        if(implicitIntent.resolveActivity(packageManager) != null) {
            return true;
        }
        else{
            return false;
        }
    }

}
