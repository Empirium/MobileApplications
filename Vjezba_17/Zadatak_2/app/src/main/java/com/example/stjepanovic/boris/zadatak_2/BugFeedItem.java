package com.example.stjepanovic.boris.zadatak_2;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 11.10.2017..
 */

@Root(name="item", strict=false)
public class BugFeedItem {
    @Element(name="title") private String mTitle;
    @Element(name="description") private String mDescription;
    @Element(name="link") private String mMore;
    @Element(name="pubDate") private String mDate;
    @Element (name = "enclosure") private BugLink mUrl;
    @Element (name = "category") private String mCategory;

    public BugFeedItem() {
    }

    public BugFeedItem(String mTitle, String mDescription, String mMore, String mDate, BugLink mUrl, String mCategory) {
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mMore = mMore;
        this.mDate = mDate;
        this.mUrl = mUrl;
        this.mCategory = mCategory;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public String getmMore() {
        return mMore;
    }

    public String getmDate() {
        return mDate;
    }

    public BugLink getmUrl() {
        return mUrl;
    }

    public String getmCategory() {
        return mCategory;
    }
}
