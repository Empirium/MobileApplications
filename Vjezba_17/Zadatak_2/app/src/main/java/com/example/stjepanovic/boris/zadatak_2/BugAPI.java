package com.example.stjepanovic.boris.zadatak_2;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by bstjepanovic on 11.10.2017..
 */

public interface BugAPI {
    String BASE_URL = "http://www.bug.hr/";

    @GET("rss/vijesti")
        Call<BugFeed> getNews();
}
