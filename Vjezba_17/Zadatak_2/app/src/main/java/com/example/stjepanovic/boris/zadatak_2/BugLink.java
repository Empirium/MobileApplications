package com.example.stjepanovic.boris.zadatak_2;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 11.10.2017..
 */

@Root(name="enclosure", strict=false)
public class BugLink {
    @Attribute(name="url") private String mURL;

    public BugLink() {
    }

    public BugLink(String mURL) {
        this.mURL = mURL;
    }

    public String getmURL() {
        return mURL;
    }
}
