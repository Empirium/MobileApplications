package com.example.stjepanovic.boris.zadatak_2;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 11.10.2017..
 */

@Root(name="rss", strict = false)
public class BugFeed {
    @Element(name="channel") BugChannel mChannel;

    public BugFeed() {
    }

    public BugFeed(BugChannel channel) {
        mChannel = channel;
    }

    public BugChannel getChannel() {
        return mChannel;
    }

}
