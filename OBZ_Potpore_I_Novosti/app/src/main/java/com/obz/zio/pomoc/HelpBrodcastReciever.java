package com.obz.zio.pomoc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by bstjepanovic on 16.11.2017..
 */

public class HelpBrodcastReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle b = intent.getExtras();
        SmsMessage[] msgs = null;
        String SMS = "";
        if (b != null){
            Object pdus[] = (Object[]) b.get("pdus");
            msgs = new SmsMessage[pdus.length];
            for (int i=0;i<pdus.length;i++){
                msgs[i] = getIncomingMessage(pdus[i], b);
                SMS += msgs[i].getMessageBody();
            }
            Toast.makeText(context, SMS, Toast.LENGTH_SHORT).show();
        }
    }

    private SmsMessage getIncomingMessage(Object aObject, Bundle bundle) {
        SmsMessage currentSMS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String format = bundle.getString("format");
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
        } else {
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject);
        }
        return currentSMS;
    }
}
