package com.obz.zio.plan_potpore;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 9.11.2017..
 */
@Root(name="enclosure", strict=false)
class PlanRSSEnclosure {
    @Attribute(name="url") private String mURL;

    public PlanRSSEnclosure() {
    }

    public PlanRSSEnclosure(String mURL) {
        this.mURL = mURL;
    }

    public String getUrl() {
        return mURL;
    }
}
