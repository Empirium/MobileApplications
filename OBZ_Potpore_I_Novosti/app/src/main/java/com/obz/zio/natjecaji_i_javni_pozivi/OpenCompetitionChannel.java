package com.obz.zio.natjecaji_i_javni_pozivi;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by bstjepanovic on 2.11.2017..
 */

@Root(name="channel", strict = false)
public class OpenCompetitionChannel {
    @ElementList(inline = true, name = "item") private List<OpenCompetitionFeedItem> mItems;

    public OpenCompetitionChannel() {}

    public OpenCompetitionChannel(List<OpenCompetitionFeedItem> items) {
        mItems = items;
    }

    public List<OpenCompetitionFeedItem> getItems() {
        return mItems;
    }
}
