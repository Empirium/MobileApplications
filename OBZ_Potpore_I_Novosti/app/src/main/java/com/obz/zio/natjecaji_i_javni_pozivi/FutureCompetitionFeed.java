package com.obz.zio.natjecaji_i_javni_pozivi;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 8.11.2017..
 */
@Root(name="rss", strict = false)
class FutureCompetitionFeed {
    @Element(name="channel") FutureCompetitionChannel mChannel;

    public FutureCompetitionFeed() {
    }

    public FutureCompetitionFeed(FutureCompetitionChannel channel) {
        mChannel = channel;
    }

    public FutureCompetitionChannel getChannel() {
        return mChannel;
    }
}
