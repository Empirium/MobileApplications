package com.obz.zio.opci_podaci;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.obz.zio.natjecaji_i_javni_pozivi.CompetitionActivity;
import com.obz.zio.novosti.NewsActivity;
import com.obz.zio.obz_potpore_i_novosti.GlavniActivity;
import com.obz.zio.obz_potpore_i_novosti.R;
import com.obz.zio.plan_potpore.PlanActivity;
import com.obz.zio.pomoc.HelpActivity;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GeneralInfoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    @BindView(R.id.tvGeneralInfoDetail) TextView tvGeneralInfoDetail;
    @BindView(R.id.btnGoogleMap) Button btnGoogleMap;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_info_navigation);
        ButterKnife.bind(this);
        toolbar.setTitle(getResources().getString(R.string.textGeneralInfo));
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @OnClick(R.id.btnGoogleMap)
    public void goToActivity(){
        Intent intent = new Intent();
        intent.setClass(this, GoogleMapActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()){
            case R.id.nav_home:
                intent.setClass(this, GlavniActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_info:
                intent.setClass(this, GeneralInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_news:
                intent.setClass(this, NewsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_plan:
                intent.setClass(this, PlanActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_competition:
                intent.setClass(this, CompetitionActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_help:
                intent.setClass(this, HelpActivity.class);
                startActivity(intent);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }
}

