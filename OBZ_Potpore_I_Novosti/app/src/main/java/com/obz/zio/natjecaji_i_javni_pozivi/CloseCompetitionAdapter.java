package com.obz.zio.natjecaji_i_javni_pozivi;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.obz.zio.obz_potpore_i_novosti.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bstjepanovic on 8.11.2017..
 */

class CloseCompetitionAdapter  extends BaseAdapter {

    List<CloseCompetitionFeedItem> mCloseItems;
    Context context;

    public CloseCompetitionAdapter(List<CloseCompetitionFeedItem> mCloseItems, Context context) {
        this.mCloseItems = mCloseItems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.mCloseItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mCloseItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CloseItemHolder itemHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_close_competition, parent, false);
            itemHolder = new CloseItemHolder(convertView);
            convertView.setTag(itemHolder);
        }else {
            itemHolder = (CloseItemHolder) convertView.getTag();
        }

        CloseCompetitionFeedItem newsItem = this.mCloseItems.get(position);

        itemHolder.tvNewsTitle.setText(newsItem.getTitle());
        itemHolder.tvNewsDescription.setText(context.getString(R.string.textDetailPurpose) + " " +   newsItem.getContent().getPurposeFor());

        return convertView;
    }

    static class CloseItemHolder{
        @BindView(R.id.tvNewsTitle) TextView tvNewsTitle;
        @BindView(R.id.tvNewsdescription) TextView tvNewsDescription;

        public CloseItemHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
