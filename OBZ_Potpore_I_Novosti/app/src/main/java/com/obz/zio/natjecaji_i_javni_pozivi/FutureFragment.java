package com.obz.zio.natjecaji_i_javni_pozivi;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.obz.zio.obz_potpore_i_novosti.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by Windows on 11/1/2017.
 */

public class FutureFragment extends Fragment implements Callback<FutureCompetitionFeed> {

    @BindView(R.id.lvFutureList) ListView lvFutureList;

    public FutureFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_future, container, false);
        ButterKnife.bind(this, view);
        this.loadData();
        return view;
    }

    private void loadData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FutureCompetitionAPI.BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        FutureCompetitionAPI futureCompetitionAPI = retrofit.create(FutureCompetitionAPI.class);
        Call<FutureCompetitionFeed> call = futureCompetitionAPI.getFutureCompetition();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<FutureCompetitionFeed> call, Response<FutureCompetitionFeed> response) {
        FutureCompetitionChannel feed = response.body().getChannel();
        List<FutureCompetitionFeedItem> items = feed.getItems();
        FutureCompetitionAdapter adapter = new FutureCompetitionAdapter(items, getContext());
        lvFutureList.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<FutureCompetitionFeed> call, Throwable t) {
        Log.e("Error", t.getMessage());
    }

    @OnItemClick(R.id.lvFutureList)
    public void futureDetails(int position) {
        FutureCompetitionFeedItem item = (FutureCompetitionFeedItem) this.lvFutureList.getAdapter().getItem(position);
        if(!item.getContent().getPurposeFor().contains(" ")) {
            Intent intent = new Intent(getActivity(), DetailCompetitionActivity.class);
            intent.putExtra(getResources().getString(R.string.keyTitle), item.getTitle());
            intent.putExtra(getResources().getString(R.string.keyPurposeFor), item.getContent().getPurposeFor());
            intent.putExtra(getResources().getString(R.string.keyLegalBasis), item.getContent().getLegalBasis());
            intent.putExtra(getResources().getString(R.string.keyUsers), item.getContent().getUsers().replace("<br>", System.getProperty("line.separator")));
            intent.putExtra(getResources().getString(R.string.keyAvTotalAmount), item.getContent().getAvTotalAmount());
            intent.putExtra(getResources().getString(R.string.keySupportAmount), item.getContent().getSupportAmount());
            intent.putExtra(getResources().getString(R.string.keyAssignment), item.getContent().getAssignment());
            intent.putExtra(getResources().getString(R.string.keyPlannedDate), item.getContent().getPlannedDate());
            intent.putExtra(getResources().getString(R.string.keyWebLink), item.getLink());
            this.startActivity(intent);
        }
    }
}
