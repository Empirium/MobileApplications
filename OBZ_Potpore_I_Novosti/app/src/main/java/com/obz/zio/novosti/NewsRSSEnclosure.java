package com.obz.zio.novosti;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 8.11.2017..
 */
@Root(name="enclosure", strict=false)
public class NewsRSSEnclosure {
    @Attribute(name="url") private String mURL;

    public NewsRSSEnclosure() {
    }

    public NewsRSSEnclosure(String mURL) {
        this.mURL = mURL;
    }

    public String getUrl() {
        return mURL;
    }
}
