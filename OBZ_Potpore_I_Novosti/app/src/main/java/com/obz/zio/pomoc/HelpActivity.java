package com.obz.zio.pomoc;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.obz.zio.natjecaji_i_javni_pozivi.CompetitionActivity;
import com.obz.zio.novosti.NewsActivity;
import com.obz.zio.obz_potpore_i_novosti.GlavniActivity;
import com.obz.zio.obz_potpore_i_novosti.R;
import com.obz.zio.opci_podaci.GeneralInfoActivity;
import com.obz.zio.plan_potpore.PlanActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HelpActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    @BindView(R.id.tvMessage) TextView tvMessage;
    @BindView(R.id.btnSendSMS) Button btnSendSMS;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;

    private static final String CUSTOM_FILTER = "custom";
    private static final String PHONE_NUMBER = "0996405699"; //0996405699
    public static final String MSG_KEY = "msg";

    HelpBrodcastReciever mCustomBroadcastReciever = new HelpBrodcastReciever();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_navigation);
        ButterKnife.bind(this);
        toolbar.setTitle(getResources().getString(R.string.textHelp));
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        Intent startingIntent = this.getIntent();
        if (startingIntent.hasExtra(HelpActivity.MSG_KEY)) {
            String message = startingIntent.getStringExtra(HelpActivity.MSG_KEY);
            this.tvMessage.setText(message);

        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()){
            case R.id.nav_home:
                intent.setClass(this, GlavniActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_info:
                intent.setClass(this, GeneralInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_news:
                intent.setClass(this, NewsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_plan:
                intent.setClass(this, PlanActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_competition:
                intent.setClass(this, CompetitionActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_help:
                intent.setClass(this, HelpActivity.class);
                startActivity(intent);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(this.mCustomBroadcastReciever, new IntentFilter(CUSTOM_FILTER));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(this.mCustomBroadcastReciever);
    }

    @OnClick(R.id.btnSendSMS)
    public void onClick(View v){
        String msg = "Jel to dovoljno za prolaz???!";
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(PHONE_NUMBER, null, msg, null, null);


        // Set-up of the required data:
        int notificationID = 1;
        String message = "Imate obavijest!";
        String title = "OBŽ novosti i potpore";
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.coat_of_arms);
        Intent intent = new Intent(this, HelpActivity.class);
        intent.putExtra(MSG_KEY, "Poslali ste SMS poruku!");
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        // Creating the notification:
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.coat_of_arms)
                .setLargeIcon(bitmap)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setLights(Color.RED, 2000, 1000)
                .setVibrate(new long[]{1000,1000,1000,1000,1000})
                .setSound(soundUri);

        // Actual notification:
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, builder.build());

        this.finish();
    }

}
