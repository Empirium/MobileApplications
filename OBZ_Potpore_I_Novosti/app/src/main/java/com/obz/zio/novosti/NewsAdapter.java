package com.obz.zio.novosti;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.obz.zio.obz_potpore_i_novosti.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Windows on 11/1/2017.
 */

public class NewsAdapter extends BaseAdapter {

    List<NewsRSSItem> mNewsItems;

    public NewsAdapter(List<NewsRSSItem> mNewsItems) {
        this.mNewsItems = mNewsItems;
    }

    @Override
    public int getCount() {
        return this.mNewsItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mNewsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NewsItemHolder itemHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_news, parent, false);
            itemHolder = new NewsItemHolder(convertView);
            convertView.setTag(itemHolder);
        } else {
            itemHolder = (NewsItemHolder) convertView.getTag();
        }

        NewsRSSItem newsItem = this.mNewsItems.get(position);


        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss Z", Locale.ENGLISH);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(newsItem.getPublishDate());
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("dd.MM.yyyy.", Locale.ENGLISH);
        String finalDate = timeFormat.format(myDate);
        itemHolder.tvNewsDate.setText(finalDate);

        itemHolder.tvNewsTitle.setText(newsItem.getTitle());
        itemHolder.tvNewsDescription.setText(newsItem.getDescription());
        Picasso.with(parent.getContext())
                .load(newsItem.getEnclosure().getUrl())
                .fit()
                .centerCrop()
                .error(R.drawable.image_unavailable)
                .placeholder(R.drawable.image_placeholder)
                .into(itemHolder.ivNewsThumbnail);
        return convertView;
    }

    static class NewsItemHolder{
        @BindView(R.id.tvNewsDate) TextView tvNewsDate;
        @BindView(R.id.tvNewsTitle) TextView tvNewsTitle;
        @BindView(R.id.tvNewsDescription) TextView tvNewsDescription;
        @BindView(R.id.ivNewsThumbnail) ImageView ivNewsThumbnail;

        public NewsItemHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
