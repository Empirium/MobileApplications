package com.obz.zio.plan_potpore;

import com.obz.zio.novosti.NewsRSSChannel;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 8.11.2017..
 */

@Root(name="rss", strict = false)
class PlanRSSFeed {
    @Element(name="channel") PlanRSSChannel mChannel;

    public PlanRSSFeed() {
    }

    public PlanRSSFeed(PlanRSSChannel channel) {
        mChannel = channel;
    }

    public PlanRSSChannel getChannel() {
        return mChannel;
    }
}
