package com.obz.zio.plan_potpore;

import com.obz.zio.novosti.NewsRSSFeed;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by bstjepanovic on 8.11.2017..
 */

interface PlanAPI {
    String BASE_URL = "http://www.obz.hr/";

    @GET("RSSMobilne/planPotpore.aspx")
    Call<PlanRSSFeed> getNews();
}
