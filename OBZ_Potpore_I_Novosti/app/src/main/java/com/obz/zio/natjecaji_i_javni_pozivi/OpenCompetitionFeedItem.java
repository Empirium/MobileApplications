package com.obz.zio.natjecaji_i_javni_pozivi;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 2.11.2017..
 */

@Root(name="item", strict=false)
public class OpenCompetitionFeedItem {
    @Element(name="guid") private String mGuid;
    @Element(name="link") private String mLink;
    @Element(name="title") private String mTitle;
    @Element (name = "description") private String mDescription;
    @Element (name = "pubDate") private String mPublishDate;
    @Element (name = "content") private CompetitionContent mCompetitionContent;

    public OpenCompetitionFeedItem() {
    }

    public OpenCompetitionFeedItem(String mGuid, String mLink, String mTitle, String mDescription, String mPublishDate, CompetitionContent mCompetitionContent) {
        this.mGuid = mGuid;
        this.mLink = mLink;
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mPublishDate = mPublishDate;
        this.mCompetitionContent = mCompetitionContent;
    }

    public String getGuid() {
        return mGuid;
    }

    public String getLink() {
        return mLink;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getPublishDate() {
        return mPublishDate;
    }

    public CompetitionContent getContent() {
        return mCompetitionContent;
    }
}
