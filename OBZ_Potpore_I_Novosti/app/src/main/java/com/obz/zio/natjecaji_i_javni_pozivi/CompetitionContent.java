package com.obz.zio.natjecaji_i_javni_pozivi;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 10.11.2017..
 */
@Root(name="content", strict=false)
class CompetitionContent {
    @Element(name="purposeFor") private String mPurposeFor;
    @Element(name="legalBasis") private String mLegalBasis;
    @Element(name="users") private String mUsers;
    @Element (name = "avTotalAmount") private String mAvTotalAmount;
    @Element (name = "supportAmount") private String mSupportAmount;
    @Element (name = "assignment") private String mAssignment;
    @Element (name = "plannedDate") private String mPlannedDate;

    public CompetitionContent() {
    }

    public CompetitionContent(String mPurposeFor, String mLegalBasis, String mUsers, String mAvTotalAmount, String mSupportAmount, String mAssignment, String mPlannedDate) {
        this.mPurposeFor = mPurposeFor;
        this.mLegalBasis = mLegalBasis;
        this.mUsers = mUsers;
        this.mAvTotalAmount = mAvTotalAmount;
        this.mSupportAmount = mSupportAmount;
        this.mAssignment = mAssignment;
        this.mPlannedDate = mPlannedDate;
    }

    public String getPurposeFor() {
        return mPurposeFor;
    }

    public String getLegalBasis() {
        return mLegalBasis;
    }

    public String getUsers() {
        return mUsers;
    }

    public String getAvTotalAmount() {
        return mAvTotalAmount;
    }

    public String getSupportAmount() {
        return mSupportAmount;
    }

    public String getAssignment() {
        return mAssignment;
    }

    public String getPlannedDate() {
        return mPlannedDate;
    }
}
