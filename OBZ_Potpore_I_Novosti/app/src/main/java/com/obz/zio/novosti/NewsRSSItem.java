package com.obz.zio.novosti;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.Date;

/**
 * Created by Windows on 11/1/2017.
 */

@Root(name="item", strict=false)
public class NewsRSSItem {
    @Element(name="guid") private String mGuid;
    @Element(name="link") private String mLink;
    @Element(name="title") private String mTitle;
    @Element(name = "description") private String mDescription;
    @Element(name = "pubDate") private String mPublishDate;
    @Element (name = "enclosure") private NewsRSSEnclosure mEnclosure;

    public NewsRSSItem() {
    }

    public NewsRSSItem(String mGuid, String mLink, String mTitle, String mDescription, String mPublishDate, NewsRSSEnclosure mEnclosure) {
        this.mGuid = mGuid;
        this.mLink = mLink;
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mPublishDate = mPublishDate;
        this.mEnclosure = mEnclosure;
    }

    public String getGuid() {
        return mGuid;
    }

    public String getLink() {
        return mLink;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getPublishDate() {
        return mPublishDate;
    }

    public NewsRSSEnclosure getEnclosure() {
        return mEnclosure;
    }
}
