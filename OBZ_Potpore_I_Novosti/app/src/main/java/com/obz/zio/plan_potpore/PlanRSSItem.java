package com.obz.zio.plan_potpore;

import com.obz.zio.novosti.NewsRSSEnclosure;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 8.11.2017..
 */
@Root(name="item", strict=false)
class PlanRSSItem {
    @Element(name="guid") private String mGuid;
    @Element(name="link") private String mLink;
    @Element(name="title") private String mTitle;
    @Element (name = "description") private String mDescription;
    @Element (name = "pubDate") private String mPublishDate;
    @Element (name = "enclosure") private PlanRSSEnclosure mEnclosure;

    public PlanRSSItem() {
    }

    public PlanRSSItem(String mGuid, String mLink, String mTitle, String mDescription, String mPublishDate, PlanRSSEnclosure mEnclosure) {
        this.mGuid = mGuid;
        this.mLink = mLink;
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mPublishDate = mPublishDate;
        this.mEnclosure = mEnclosure;
    }

    public String getGuid() {
        return mGuid;
    }

    public String getLink() {
        return mLink;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getPublishDate() {
        return mPublishDate;
    }

    public PlanRSSEnclosure getEnclosure() {
        return mEnclosure;
    }
}
