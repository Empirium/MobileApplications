package com.obz.zio.natjecaji_i_javni_pozivi;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by bstjepanovic on 8.11.2017..
 */

public interface CloseCompetitionAPI {
    String BASE_URL = "http://www.obz.hr/";

    @GET("RSSMobilne/zavrseni.aspx")
    Call<CloseCompetitionFeed> getCloseCompetition();
}
