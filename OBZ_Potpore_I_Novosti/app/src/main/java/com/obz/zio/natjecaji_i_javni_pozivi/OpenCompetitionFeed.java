package com.obz.zio.natjecaji_i_javni_pozivi;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 2.11.2017..
 */

@Root(name="rss", strict = false)
public class OpenCompetitionFeed {
    @Element(name="channel") OpenCompetitionChannel mChannel;

    public OpenCompetitionFeed() {
    }

    public OpenCompetitionFeed(OpenCompetitionChannel channel) {
        mChannel = channel;
    }

    public OpenCompetitionChannel getChannel() {
        return mChannel;
    }
}
