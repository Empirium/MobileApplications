package com.obz.zio.natjecaji_i_javni_pozivi;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by bstjepanovic on 8.11.2017..
 */
@Root(name="channel", strict = false)
class FutureCompetitionChannel {
    @ElementList(inline = true, name = "item") private List<FutureCompetitionFeedItem> mItems;

    public FutureCompetitionChannel() {}

    public FutureCompetitionChannel(List<FutureCompetitionFeedItem> items) {
        mItems = items;
    }

    public List<FutureCompetitionFeedItem> getItems() {
        return mItems;
    }
}
