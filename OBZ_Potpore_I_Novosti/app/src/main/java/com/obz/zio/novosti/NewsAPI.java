package com.obz.zio.novosti;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Windows on 11/1/2017.
 */

public interface NewsAPI {
    String BASE_URL = "http://www.obz.hr/";

    @GET("RSSMobilne/novosti.aspx")
        Call<NewsRSSFeed> getNews();
}
