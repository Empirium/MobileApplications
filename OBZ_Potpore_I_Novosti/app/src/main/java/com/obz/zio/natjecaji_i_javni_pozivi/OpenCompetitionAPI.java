package com.obz.zio.natjecaji_i_javni_pozivi;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by bstjepanovic on 2.11.2017..
 */

public interface OpenCompetitionAPI {
    String BASE_URL = "http://www.obz.hr/";

    @GET("RSSMobilne/otvoreni.aspx")
    Call<OpenCompetitionFeed> getOpenCompetition();
}
