package com.obz.zio.plan_potpore;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.obz.zio.obz_potpore_i_novosti.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bstjepanovic on 8.11.2017..
 */

class PlanAdapter extends BaseAdapter {

    List<PlanRSSItem> mPlanItems;

    public PlanAdapter(List<PlanRSSItem> mPlanItems) {
        this.mPlanItems = mPlanItems;
    }

    @Override
    public int getCount() {
        return this.mPlanItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mPlanItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PlanItemHolder itemHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_plan, parent, false);
            itemHolder = new PlanItemHolder(convertView);
            convertView.setTag(itemHolder);
        }else {
            itemHolder = (PlanItemHolder) convertView.getTag();
        }

        PlanRSSItem planItem = this.mPlanItems.get(position);

        itemHolder.tvPlanTitle.setText(planItem.getTitle());
        itemHolder.tvPlanDescription.setText(planItem.getDescription());

        return convertView;
    }

    static class PlanItemHolder{
        @BindView(R.id.tvPlanTitle) TextView tvPlanTitle;
        @BindView(R.id.tvPlanDescription) TextView tvPlanDescription;
        @BindView(R.id.btnPlanPDF) Button btnPlanPDF;

        public PlanItemHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
