package com.obz.zio.obz_potpore_i_novosti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.obz.zio.opci_podaci.*;
import com.obz.zio.novosti.*;
import com.obz.zio.plan_potpore.*;
import com.obz.zio.natjecaji_i_javni_pozivi.*;

public class GlavniActivity extends AppCompatActivity {

    @BindView(R.id.btnGeneralInfo) Button btnGeneralInfo;
    @BindView(R.id.btnNews) Button btnNews;
    @BindView(R.id.btnPlan) Button btnPlan;
    @BindView(R.id.btnCompetition) Button btnCompetition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glavni);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnGeneralInfo, R.id.btnNews, R.id.btnPlan, R.id.btnCompetition})
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()){
            case R.id.btnGeneralInfo:
                intent.setClass(this, GeneralInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.btnNews:
                intent.setClass(this, NewsActivity.class);
                startActivity(intent);
                break;
            case R.id.btnPlan:
                intent.setClass(this, PlanActivity.class);
                startActivity(intent);
                break;
            case R.id.btnCompetition:
                intent.setClass(this, CompetitionActivity.class);
                startActivity(intent);
                break;
        }
    }
}
