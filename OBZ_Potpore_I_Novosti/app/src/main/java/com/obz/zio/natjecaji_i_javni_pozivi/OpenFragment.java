package com.obz.zio.natjecaji_i_javni_pozivi;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.obz.zio.obz_potpore_i_novosti.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by Windows on 11/1/2017.
 */

public class OpenFragment extends Fragment implements Callback<OpenCompetitionFeed> {

    @BindView(R.id.lvOpenList) ListView lvOpenList;

    public OpenFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_open, container, false);
        ButterKnife.bind(this, view);
        this.loadData();
        return view;
    }

    private void loadData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(OpenCompetitionAPI.BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        OpenCompetitionAPI openCompetitionAPI = retrofit.create(OpenCompetitionAPI.class);
        Call<OpenCompetitionFeed> call = openCompetitionAPI.getOpenCompetition();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<OpenCompetitionFeed> call, Response<OpenCompetitionFeed> response) {
        OpenCompetitionChannel feed = response.body().getChannel();
        List<OpenCompetitionFeedItem> items = feed.getItems();
        OpenCompetitionAdapter adapter = new OpenCompetitionAdapter(items, getContext());
        lvOpenList.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<OpenCompetitionFeed> call, Throwable t) {
        Log.e("Error", t.getMessage());
    }

    @OnItemClick(R.id.lvOpenList)
    public void openDetails(int position) {
        OpenCompetitionFeedItem item = (OpenCompetitionFeedItem) this.lvOpenList.getAdapter().getItem(position);
        if(item.getContent().getPurposeFor().contains(" ")) {
            Intent intent = new Intent(getActivity(), DetailCompetitionActivity.class);
            intent.putExtra(getResources().getString(R.string.keyTitle), item.getTitle());
            intent.putExtra(getResources().getString(R.string.keyPurposeFor), item.getContent().getPurposeFor());
            intent.putExtra(getResources().getString(R.string.keyLegalBasis), item.getContent().getLegalBasis());
            intent.putExtra(getResources().getString(R.string.keyUsers), item.getContent().getUsers());
            intent.putExtra(getResources().getString(R.string.keyAvTotalAmount), item.getContent().getAvTotalAmount());
            intent.putExtra(getResources().getString(R.string.keySupportAmount), item.getContent().getSupportAmount());
            intent.putExtra(getResources().getString(R.string.keyAssignment), item.getContent().getAssignment());
            intent.putExtra(getResources().getString(R.string.keyPlannedDate), item.getContent().getPlannedDate());
            intent.putExtra(getResources().getString(R.string.keyWebLink), item.getLink());
            this.startActivity(intent);
        }
    }
}
