package com.obz.zio.natjecaji_i_javni_pozivi;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.obz.zio.novosti.NewsActivity;
import com.obz.zio.obz_potpore_i_novosti.GlavniActivity;
import com.obz.zio.obz_potpore_i_novosti.R;
import com.obz.zio.opci_podaci.GeneralInfoActivity;
import com.obz.zio.plan_potpore.PlanActivity;
import com.obz.zio.pomoc.HelpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnPageChange;

public class CompetitionActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.vpPager) ViewPager vpPager;
    @BindView(R.id.tlTabs) TabLayout tlTabs;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competition_navigation);
        ButterKnife.bind(this);
        toolbar.setTitle(getResources().getString(R.string.textCompetition));
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        this.loadFragments();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_news_items, menu);
        return true;
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.miRefresh:
                this.loadData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    private void loadFragments() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.insertFragment(new OpenFragment(), getResources().getString(R.string.textCompetitionOpen));
        adapter.insertFragment(new FutureFragment(), getResources().getString(R.string.textCompetitionFuture));
        adapter.insertFragment(new CloseFragment(), getResources().getString(R.string.textCompetitionClose));
        this.vpPager.setAdapter(adapter);
        this.tlTabs.setupWithViewPager(this.vpPager);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()){
            case R.id.nav_home:
                intent.setClass(this, GlavniActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_info:
                intent.setClass(this, GeneralInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_news:
                intent.setClass(this, NewsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_plan:
                intent.setClass(this, PlanActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_competition:
                intent.setClass(this, CompetitionActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_help:
                intent.setClass(this, HelpActivity.class);
                startActivity(intent);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }
}
