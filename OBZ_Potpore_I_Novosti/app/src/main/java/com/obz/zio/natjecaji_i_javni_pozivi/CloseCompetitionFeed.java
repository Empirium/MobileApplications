package com.obz.zio.natjecaji_i_javni_pozivi;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bstjepanovic on 8.11.2017..
 */

@Root(name="rss", strict = false)
class CloseCompetitionFeed {
    @Element(name="channel") CloseCompetitionChannel mChannel;

    public CloseCompetitionFeed() {
    }

    public CloseCompetitionFeed(CloseCompetitionChannel channel) {
        mChannel = channel;
    }

    public CloseCompetitionChannel getChannel() {
        return mChannel;
    }
}
