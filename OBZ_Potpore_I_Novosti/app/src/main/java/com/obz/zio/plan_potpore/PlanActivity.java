package com.obz.zio.plan_potpore;

import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.obz.zio.natjecaji_i_javni_pozivi.CompetitionActivity;
import com.obz.zio.novosti.NewsAPI;
import com.obz.zio.novosti.NewsActivity;
import com.obz.zio.novosti.NewsAdapter;
import com.obz.zio.novosti.NewsRSSChannel;
import com.obz.zio.novosti.NewsRSSFeed;
import com.obz.zio.novosti.NewsRSSItem;
import com.obz.zio.obz_potpore_i_novosti.GlavniActivity;
import com.obz.zio.obz_potpore_i_novosti.R;
import com.obz.zio.opci_podaci.GeneralInfoActivity;
import com.obz.zio.pomoc.HelpActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class PlanActivity extends AppCompatActivity implements Callback<PlanRSSFeed>, NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.lvPlan) ListView lvPlan;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_navigation);
        ButterKnife.bind(this);
        toolbar.setTitle(getResources().getString(R.string.textPlan));
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        this.loadData();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_plan_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.miRefresh:
                this.loadData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PlanAPI.BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        PlanAPI planAPI = retrofit.create(PlanAPI.class);
        Call<PlanRSSFeed> call = planAPI.getNews();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<PlanRSSFeed> call, Response<PlanRSSFeed> response) {
        PlanRSSChannel feed = response.body().getChannel();
        List<PlanRSSItem> items = feed.getItems();
        PlanAdapter adapter = new PlanAdapter(items);
        lvPlan.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<PlanRSSFeed> call, Throwable t) {
        Log.e("Error", t.getMessage());
    }

    private boolean canBeCalled(Intent implicitIntent){
        PackageManager packageManager = this.getPackageManager();
        if(implicitIntent.resolveActivity(packageManager) != null) {
            return true;
        }
        else{
            return false;
        }
    }

    @OnItemClick(R.id.lvPlan)
    public void openPDF(int position){
        PlanRSSItem item = (PlanRSSItem) this.lvPlan.getAdapter().getItem(position);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getLink()));
        if(canBeCalled(intent)){
            startActivity(intent);
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()){
            case R.id.nav_home:
                intent.setClass(this, GlavniActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_info:
                intent.setClass(this, GeneralInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_news:
                intent.setClass(this, NewsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_plan:
                intent.setClass(this, PlanActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_competition:
                intent.setClass(this, CompetitionActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_help:
                intent.setClass(this, HelpActivity.class);
                startActivity(intent);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }
}
