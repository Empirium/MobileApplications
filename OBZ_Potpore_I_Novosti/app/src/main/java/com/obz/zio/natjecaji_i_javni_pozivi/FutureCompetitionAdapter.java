package com.obz.zio.natjecaji_i_javni_pozivi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.obz.zio.obz_potpore_i_novosti.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bstjepanovic on 8.11.2017..
 */

class FutureCompetitionAdapter extends BaseAdapter {

    List<FutureCompetitionFeedItem> mFutureItems;
    Context context;

    public FutureCompetitionAdapter(List<FutureCompetitionFeedItem> mFutureItems, Context context) {
        this.mFutureItems = mFutureItems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.mFutureItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mFutureItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FutureItemHolder itemHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_future_competition, parent, false);
            itemHolder = new FutureItemHolder(convertView);
            convertView.setTag(itemHolder);
        }else {
            itemHolder = (FutureItemHolder) convertView.getTag();
        }

        FutureCompetitionFeedItem newsItem = this.mFutureItems.get(position);

        itemHolder.tvNewsTitle.setText(newsItem.getTitle());
        if(!newsItem.getContent().getPurposeFor().contains(" ")) {
            itemHolder.tvNewsDescription.setText(context.getString(R.string.textDetailPurpose) + " " + newsItem.getContent().getPurposeFor());
        }
        else{
            itemHolder.tvNewsDescription.setText(newsItem.getContent().getPurposeFor());
        }

        return convertView;
    }

    static class FutureItemHolder{
        @BindView(R.id.tvNewsTitle) TextView tvNewsTitle;
        @BindView(R.id.tvNewsdescription) TextView tvNewsDescription;

        public FutureItemHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
