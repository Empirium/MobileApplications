package com.obz.zio.plan_potpore;

import com.obz.zio.novosti.NewsRSSItem;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by bstjepanovic on 8.11.2017..
 */
@Root(name="channel", strict = false)
class PlanRSSChannel {
    @ElementList(inline = true, name = "item") private List<PlanRSSItem> mItems;

    public PlanRSSChannel() {}

    public PlanRSSChannel(List<PlanRSSItem> items) {
        mItems = items;
    }

    public List<PlanRSSItem> getItems() {
        return mItems;
    }
}
