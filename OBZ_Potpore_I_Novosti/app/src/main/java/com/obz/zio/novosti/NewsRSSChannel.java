package com.obz.zio.novosti;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Windows on 11/1/2017.
 */

@Root(name="channel", strict = false)
public class NewsRSSChannel {
    @ElementList(inline = true, name = "item") private List<NewsRSSItem> mItems;

    public NewsRSSChannel() {}

    public NewsRSSChannel(List<NewsRSSItem> items) {
        mItems = items;
    }

    public List<NewsRSSItem> getItems() {
        return mItems;
    }
}
