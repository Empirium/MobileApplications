package com.obz.zio.natjecaji_i_javni_pozivi;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.obz.zio.novosti.NewsActivity;
import com.obz.zio.obz_potpore_i_novosti.GlavniActivity;
import com.obz.zio.obz_potpore_i_novosti.R;
import com.obz.zio.opci_podaci.GeneralInfoActivity;
import com.obz.zio.plan_potpore.PlanActivity;
import com.obz.zio.pomoc.HelpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailCompetitionActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.tvPurposeFor) TextView tvPurposeFor;
    @BindView(R.id.tvLegalBasis) TextView tvLegalBasis;
    @BindView(R.id.tvUsers) TextView tvUsers;
    @BindView(R.id.tvAvTotalAmount) TextView tvAvTotalAmount;
    @BindView(R.id.tvSupportAmount) TextView tvSupportAmount;
    @BindView(R.id.tvAssignment) TextView tvAssignment;
    @BindView(R.id.tvPlannedDate) TextView tvPlannedDate;
    @BindView(R.id.btnWebLink) Button btnWebLink;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;

    private android.support.v7.widget.ShareActionProvider mShareActionProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_competiton_navigation);
        ButterKnife.bind(this);
        this.handleStartingIntent(this.getIntent());
        toolbar.setTitle(this.getIntent().getStringExtra(getResources().getString(R.string.keyTitle)));
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details_items, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);
        mShareActionProvider = (android.support.v7.widget.ShareActionProvider) MenuItemCompat.getActionProvider(item);
        return true;
    }

    public Intent doShare() {
        // populate the share intent with data
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, this.getIntent().getStringExtra(getResources().getString(R.string.keyWebLink)));
        startActivity(Intent.createChooser(sharingIntent, "Podijeli sa"));
        return sharingIntent;
    }

    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_share:
                this.setShareIntent(doShare());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void handleStartingIntent(Intent intent) {
        if (intent != null) {
            if (intent.hasExtra(getResources().getString(R.string.keyPurposeFor))) {
                tvPurposeFor.setText(intent.getStringExtra(getResources().getString(R.string.keyPurposeFor)));
            }
            if (intent.hasExtra(getResources().getString(R.string.keyLegalBasis))) {
                tvLegalBasis.setText(intent.getStringExtra(getResources().getString(R.string.keyLegalBasis)));
            }
            if (intent.hasExtra(getResources().getString(R.string.keyUsers))) {
                tvUsers.setText(intent.getStringExtra(getResources().getString(R.string.keyUsers)));
            }
            if (intent.hasExtra(getResources().getString(R.string.keyAvTotalAmount))) {
                tvAvTotalAmount.setText(intent.getStringExtra(getResources().getString(R.string.keyAvTotalAmount)));
            }
            if(intent.hasExtra(getResources().getString(R.string.keySupportAmount))){
                tvSupportAmount.setText(intent.getStringExtra(getResources().getString(R.string.keySupportAmount)));
            }
            if(intent.hasExtra(getResources().getString(R.string.keyAssignment))){
                tvAssignment.setText(intent.getStringExtra(getResources().getString(R.string.keyAssignment)));
            }
            if(intent.hasExtra(getResources().getString(R.string.keyPlannedDate))){
                tvPlannedDate.setText(intent.getStringExtra(getResources().getString(R.string.keyPlannedDate)));
            }
        }
    }

    @OnClick(R.id.btnWebLink)
    public void webLink(){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(getIntent().getStringExtra(getResources().getString(R.string.keyWebLink))));
        startActivity(i);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()){
            case R.id.nav_home:
                intent.setClass(this, GlavniActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_info:
                intent.setClass(this, GeneralInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_news:
                intent.setClass(this, NewsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_plan:
                intent.setClass(this, PlanActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_competition:
                intent.setClass(this, CompetitionActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_help:
                intent.setClass(this, HelpActivity.class);
                startActivity(intent);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }
}
