package com.obz.zio.novosti;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Windows on 11/1/2017.
 */

@Root(name="rss", strict = false)
public class NewsRSSFeed {
    @Element(name="channel") NewsRSSChannel mChannel;

    public NewsRSSFeed() {
    }

    public NewsRSSFeed(NewsRSSChannel channel) {
        mChannel = channel;
    }

    public NewsRSSChannel getChannel() {
        return mChannel;
    }
}
