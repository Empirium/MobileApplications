package com.obz.zio.natjecaji_i_javni_pozivi;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by bstjepanovic on 8.11.2017..
 */
@Root(name="channel", strict = false)
class CloseCompetitionChannel {
    @ElementList(inline = true, name = "item") private List<CloseCompetitionFeedItem> mItems;

    public CloseCompetitionChannel() {}

    public CloseCompetitionChannel(List<CloseCompetitionFeedItem> items) {
        mItems = items;
    }

    public List<CloseCompetitionFeedItem> getItems() {
        return mItems;
    }
}
