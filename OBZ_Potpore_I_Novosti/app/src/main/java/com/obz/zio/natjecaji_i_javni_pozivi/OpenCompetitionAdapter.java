package com.obz.zio.natjecaji_i_javni_pozivi;

import android.content.Context;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.obz.zio.obz_potpore_i_novosti.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bstjepanovic on 2.11.2017..
 */

public class OpenCompetitionAdapter extends BaseAdapter {

    List<OpenCompetitionFeedItem> mOpenItems;
    Context context;

    public OpenCompetitionAdapter(List<OpenCompetitionFeedItem> mNewsItems, Context context) {
        this.mOpenItems = mNewsItems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.mOpenItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mOpenItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return  position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OpenItemHolder itemHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_open_competition, parent, false);
            itemHolder = new OpenItemHolder(convertView);
            convertView.setTag(itemHolder);
        }else {
            itemHolder = (OpenItemHolder) convertView.getTag();
        }

        OpenCompetitionFeedItem newsItem = this.mOpenItems.get(position);

        itemHolder.tvNewsTitle.setText(newsItem.getTitle());
        itemHolder.tvNewsDescription.setText(context.getString(R.string.textDetailPurpose) + " " +   newsItem.getContent().getPurposeFor());

        return convertView;
    }

    static class OpenItemHolder{
        @BindView(R.id.tvNewsTitle) TextView tvNewsTitle;
        @BindView(R.id.tvNewsdescription) TextView tvNewsDescription;

        public OpenItemHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
